<%@ page import="com.toro.esb.core.esbpackage.model.ESBPackage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%
    ESBPackage esbPackage = ( ESBPackage ) getServletConfig().getServletContext().getAttribute( "ESB_PACKAGE" );
    // if you need to use the package or package name for any hyperlinks, here it is.
    String packageName = esbPackage.getName();
%>

<head>
    <meta charset="utf-8">
    <title>TORO Example package</title>
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" target="_blank" href="/assets/css/uikit/uikit.css">
    <link rel="stylesheet" href="/assets/css/uikit/uikit-components.css">
    <link rel="stylesheet" href="/assets/css/uikit/uikit-docs.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">


    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700">
    <link rel="stylesheet" href="/assets/css/bootstrap/datetimepicker.css">
    <link rel="stylesheet" href="/assets/css/layout-esb.css">
    <link rel="stylesheet" href="/assets/css/layout-error_404.css">
    <!-- jQuery and jQueryUI -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!-- globalize for graphs -->
    <script src="//ajax.aspnetcdn.com/ajax/globalize/0.1.1/globalize.min.js"></script>
    <script src="/assets/js/vendor/jquery.flot.js"></script>
    <script src="/assets/js/vendor/uikit-flot.js"></script>
    <script src="/assets/js/plugins/moment.js"></script>
    <!-- Bootstrap Plugins -->
    <script src="/assets/js/bootstrap/bootstrap.js"></script>
    <script src="/assets/js/bootstrap/datetimepicker.min.js"></script>
    <!-- UIKit Plugins -->
    <script src="/assets/js/plugins/prettify.js"></script>
    <script src="/assets/js/plugins/plugin.js"></script>
    <script src="/assets/js/plugins/tableSorter.js"></script>
    <script src="/assets/js/plugins/pagination.js"></script>
    <script src="/assets/js/plugins/sticky-navbar.js"></script>
    <script src="/assets/js/plugins/jquery.tooltipster.min.js"></script>
    <script src="/assets/js/plugins/hoverChart.js"></script>
    <script src="/assets/js/docs.js"></script>
    <script src="/assets/esb/js/esb.js"></script>
</head>
<body style="position: relative;">
<div class="bs-docs-header" id="content" style="background-image: linear-gradient(to bottom, #2DBE60 0%, #a4e9bc 100%);">
    <div class="container" id="top">
        <h1 style="width: 100%">TORO INTEGRATE - Evernote Adapter</h1>

        <p style="color:#807C7C">Read below on how to write beautiful code that seamlessly integrates the TORO INTEGRATE with Evernote.</p>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-9" role="main">
            <div class="bs-docs-section">
                <h1 id="overview" class="page-header">
                    Overview
                </h1>

                <p>
                    The TORO INTEGRATE Evernote adapter is a small <a href="http://groovy.codehaus.org/">Groovy</a> script
                    that allows you to create and manage notes, collaborate to your colleagues or even to other public users. You can even share your notes, or notebooks to public via email or links!
                    Once configured, it's literally a 'one-liner', as shown below.
                    <br/>
                    <br/>
                <pre class="prettyprint lang-groovy linenums">'myEvernote'.evernoteListNotebooks()</pre>
            </div>
            <div class="bs-docs-section">
                <h1 id="authenticating" class="page-header">Authenticating</h1>

                <p>
                    There are two ways to authenticate to Evernote API, through <a href="https://dev.evernote.com/doc/articles/dev_tokens.php">Developer Tokens</a> and <a href="https://dev.evernote.com/doc/articles/authentication.php">OAuth</a>. Evernote also provide sandbox and production services, so it is must to ensure what credentials are to save.
                </p>

                <h3 id="getting-dev-keys">Getting Developer Tokens</h3>

                <p>
                    Developer Tokens are available for Evernote sandbox and production services. Once you've fully accomplished creating an Evernote account, you can now get developer token:
                    <ul>
                        <li><a href="https://sandbox.evernote.com/api/DeveloperToken.action">Sandbox</a></li>
                        <li><a href="https://www.evernote.com/api/DeveloperToken.action">Production</a></li>
                    </ul>
                </p>

                <h3 id="getting-oauth-keys">Getting OAuth Keys</h3>
                <p>
                    Evernote OAuth is usually used for public applications, webhook notifications, and advance permissions. First, you must apply for an <a href="https://dev.evernote.com/doc/">API Key</a>. Enter your desired application name, and when success, an email will be sent containing your Consumer Key, and Secret.
                </p>
                <h4>Step 1. Save Evernote credentials</h4>

                <p>
                    Once you've finally registered your account and acquired your credentials, you can now save necessary credentials in to TORO INTEGRATE package by invoking a service called
                        <a target="_blank"
                        href="/invoker/get/<%=packageName%>/groovy%3Aio.toro.integrate.evernote.EvernoteRegister/public+java.lang.Object+io.toro.integrate.evernote.EvernoteRegister.saveAlias%28java.lang.String%2Cjava.lang.String%2Cjava.lang.String%2Cjava.lang.String%2Cjava.lang.String%2Cjava.lang.String%2Cjava.lang.String%29+throws+java.lang.Exception">saveAlias</a>.
                        First, you must specify what kind of service is your credentials, you can choose from the dropdown <strong>serviceType</strong> whether it's a Sandbox or Production.
                        Second, you must also specify what type of authentication you are saving. You can choose from the dropdown <strong>OAuthType</strong> whether it's a Developer Token or OAuth. 
                </p>
                <p>
                    If you've selected <strong>Developer Token</strong>, then it is a must to fill outh the field <strong>developerToken</strong>, and leave the <strong>consumerKey</strong>, <strong>consumerSecret</strong>, and <strong>redirectUri</strong> blank. In contrary, if you chose <strong>OAuth</strong>, you must fill out the fields <strong>consumerKey</strong>, <strong>consumerSecret</strong>, and <strong>redirectUri</strong>, and leave the <strong>developerToken</strong> field blank.
                    Lastly, the <strong>alias</strong> field should be something easy to remember, as it's used to tell the TORO INTEGRATE which Evernote account to use at runtime. For example, if you use the alias <code>test</code>, then you will create againts the account with the following code:
                    <pre class="prettyprint lang-groovy linenums">'test'.ervernoteListNotebooks()</pre>
                </p>

                <br/>
                <h4>Step 2. Get Access Token</h4>
                <p>  
                    If OAuth is chosen as authentication, then this must be completed. 
                    After saving all the credentials, you will be redirected to a page where you will authorise the application requested to the Evernote. After authorising, a <strong>verifierCode</strong> will be given which will be in exchange for Access Token.
                </p>
                <p>
                    To get the Access token, invoke a TORO INTEGRATE service called 
                    <a target="_blank" 
                    href="/invoker/get/<%=packageName%>/groovy%3Aio.toro.integrate.evernote.EvernoteRegister/public+com.toro.esb.core.api.APIResponse+io.toro.integrate.evernote.EvernoteRegister.getToken%28java.lang.String%2Cjava.lang.String%29">getToken</a>.
                    Enter the <strong>alias</strong> used for the Evernote account, and the unique <strong>verifierCode</strong> 
                    Once invoked this will respond OK and will give how your Access token looks like, and it will be automatically save in the TORO INTEGRATE package.
                </p>
            </div>
            <div class="bs-docs-section">
                <h1 id="usage" class="page-header">
                    Usage
                </h1>
                The TORO INTEGRATE Evernote adapter comes with small, easy to use methods. The <code>createNote</code> allows you to create a new against your Evernote account.
                The <code>listNotebooks</code> method allows you to retrieve all existing notebooks in your account
                You can test the services here:
                <ul>
                    <li>
                        <a target="_blank" 
                            href="/invoker/get/<%=packageName%>/groovy%3Aio.toro.integrate.evernote.EvernoteAdapter/public+com.toro.esb.core.api.APIResponse+io.toro.integrate.evernote.EvernoteAdapter.createNote%28java.lang.String%2Cjava.io.File%2Cjava.lang.String%2Ccom.evernote.edam.type.ResourceAttributes%2Ccom.evernote.edam.type.Note%2Cjava.lang.String%29">createNote</a>
                    </li>
                    <li>
                        <a target="_blank"
                           href="/invoker/get/<%=packageName%>/groovy%3Aio.toro.integrate.evernote.EvernoteAdapter/public+com.toro.esb.core.api.APIResponse+io.toro.integrate.evernote.EvernoteAdapter.listNotebooks%28java.lang.String%29">listNotebooks</a>
                    </li>
                </ul>

            </div>

        </div>
        <div class="col-md-3">
            <div class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix-top" role="complementary">
                <ul class="nav bs-docs-sidenav">
                    <li class="active"><a href="#overview">Overview</a></li>
                    <li>
                        <a href="#authenticating">Authenticating</a>
                    </li>
                    <li>
                        <a href="#usage">Usage</a>
                    </li>
                </ul>
                <a class="back-to-top" href="#top">
                    Back to top
                </a>
            </div>
        </div>
    </div>
</div>
<%--script type="text/javascript">
    window.prettyPrint && prettyPrint();
</script--%>
</body>
</html>