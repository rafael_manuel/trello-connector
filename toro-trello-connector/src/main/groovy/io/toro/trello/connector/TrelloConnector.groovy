package io.toro.trello.connector

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import groovyx.net.http.ContentType
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.HttpResponseException
import groovyx.net.http.Method
import groovyx.net.http.ParserRegistry
import groovyx.net.http.RESTClient
import io.toro.trello.exception.TrelloException
import io.toro.trello.model.boards.AddBoard
import io.toro.trello.model.boards.BoardAction
import io.toro.trello.model.boards.BoardCard
import io.toro.trello.model.boards.BoardCheckItems
import io.toro.trello.model.boards.BoardChecklist
import io.toro.trello.model.boards.BoardList
import io.toro.trello.model.boards.BoardMember
import io.toro.trello.model.boards.BoardPrefs
import io.toro.trello.model.boards.BoardStars
import io.toro.trello.model.boards.Boards
import io.toro.trello.model.boards.CalendarEmailKey
import io.toro.trello.model.boards.OrgPrefs
import io.toro.trello.model.boards.Organization
import io.toro.trello.model.boards.UpdateBoard
import io.toro.trello.model.cards.AddCard
import io.toro.trello.model.cards.Attachment
import io.toro.trello.model.cards.Badges
import io.toro.trello.model.cards.Card
import io.toro.trello.model.cards.CardBoard
import io.toro.trello.model.cards.CheckItemState
import io.toro.trello.model.cards.Label
import io.toro.trello.model.cards.Stickers
import io.toro.trello.model.cards.UpdateCard
import io.toro.trello.model.fields.LabelNames
import io.toro.trello.model.fields.Membership
import io.toro.trello.model.fields.Prefs
import io.toro.trello.model.query.ActionQuery
import io.toro.trello.model.query.BoardActionQuery
import io.toro.trello.model.query.BoardCardQuery
import io.toro.trello.model.query.BoardChecklistQuery
import io.toro.trello.model.query.BoardQuery
import io.toro.trello.model.query.CardQuery
import io.toro.trello.model.query.ListQuery
import java.nio.charset.Charset
import org.apache.http.HttpResponse
import org.apache.http.entity.StringEntity
import org.codehaus.groovy.tools.shell.commands.AliasCommand;
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.AnnotationIntrospector
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector
import com.fasterxml.jackson.databind.type.TypeFactory
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector
import io.toro.trello.model.checklists.Checklists
import io.toro.trello.model.checklists.CheckItem
import io.toro.trello.model.checklists.FieldQuery
import io.toro.trello.model.checklists.ChecklistCard
import io.toro.trello.model.checklists.AddChecklist
import io.toro.trello.model.checklists.AddCheckItem
import io.toro.trello.model.labels.Labels
import io.toro.trello.model.labels.LabelBoard
import io.toro.trello.model.query.LabelQuery
import io.toro.trello.model.lists.Lists
import io.toro.trello.model.lists.ListsQuery
import io.toro.trello.model.query.ListCardQuery
import io.toro.trello.model.members.Member
import io.toro.trello.model.members.MemberActions
import io.toro.trello.model.members.MemberData
import io.toro.trello.model.members.MemberCreator
import io.toro.trello.model.query.SearchQuery
import io.toro.trello.model.search.SearchMember
import io.toro.trello.model.query.SearchMemberQuery
import io.toro.trello.model.types.Types
import io.toro.trello.model.organizations.Organizations
import io.toro.trello.model.query.OrganizationQuery
import io.toro.trello.model.organizations.OrganizationActions
import io.toro.trello.model.request.RequestOrganizationActions
import io.toro.trello.model.organizations.OrganizationBoards
import io.toro.trello.request.OrganizationBoardRequest
import io.toro.trello.request.OrganizationMembershipRequest
import io.toro.trello.request.TokenWebhookRequest
import io.toro.trello.model.tokens.Token
import io.toro.trello.model.tokens.TokenMember
import io.toro.trello.model.tokens.TokenWebhook
import io.toro.trello.model.webhooks.Webhook
import java.util.List
import io.toro.trello.model.members.MemberNotification
import io.toro.trello.response.NotificationResponse
import io.toro.trello.request.NotificationRequest
import io.toro.trello.response.NotificationFieldResponse
import io.toro.trello.request.NotificationFieldRequest
import io.toro.trello.response.NotificationEntitiesResponse
import io.toro.trello.response.NotificationMemberCreatorResponse
import io.toro.trello.request.NotificationMemberCreatorRequest
import io.toro.trello.request.NotificationMemberCreatorFieldRequest
import io.toro.trello.response.NotificationMemberCreatorFieldResponse
import io.toro.trello.response.NotificationOrganizationResponse
import io.toro.trello.response.NotificationOrganizationFieldResponse
import io.toro.trello.request.NotificationOrganizationRequest
import io.toro.trello.request.NotificationOrganizationFieldRequest
import io.toro.trello.request.NotificationFieldUnreadRequest
import io.toro.trello.response.SearchResponse
import io.toro.trello.response.SearchMemberResponse
import io.toro.trello.request.SearchRequest
import io.toro.trello.request.SearchMemberRequest
import io.toro.trello.request.TypeRequest
import io.toro.trello.response.TypeResponse
import io.toro.trello.response.MemberOrganizationResponse
import io.toro.trello.request.MemberOrganizationRequest
import io.toro.trello.response.MemberOrganizationFilterResponse
import io.toro.trello.request.MemberOrganizationFilterRequest
import io.toro.trello.request.MemberOrganizationInvitedRequest
import io.toro.trello.request.MemberOrganizationInvitedFieldRequest
import io.toro.trello.response.MemberOrganizationInvitedFieldResponse
import io.toro.trello.response.MemberSavedSearchesResponse
import io.toro.trello.request.MemberSavedSearchesRequest
import io.toro.trello.request.*
import io.toro.trello.response.*
import io.toro.trello.response.ActionResponse
import io.toro.trello.request.ActionRequest
import io.toro.trello.response.ActionFieldResponse
import io.toro.trello.request.ActionFieldRequest
import io.toro.trello.response.ActionBoardResponse
import io.toro.trello.request.ActionBoardRequest
import io.toro.trello.response.ActionBoardFieldResponse
import io.toro.trello.request.ActionBoardFieldRequest
import io.toro.trello.response.ActionCardResponse
import io.toro.trello.request.ActionCardRequest
import io.toro.trello.response.ActionDisplayResponse
import io.toro.trello.response.ActionEntitiesResponse
import io.toro.trello.response.ActionCardFieldResponse
import io.toro.trello.request.ActionCardFieldRequest
import io.toro.trello.response.ActionListResponse
import io.toro.trello.request.ActionListRequest
import io.toro.trello.response.ActionListFieldResponse
import io.toro.trello.request.ActionListFieldRequest
import io.toro.trello.response.ActionMemberResponse
import io.toro.trello.request.ActionMemberRequest
import io.toro.trello.response.ActionMemberFieldResponse
import io.toro.trello.request.ActionMemberFieldRequest
import io.toro.trello.response.ActionMemberCreatorResponse
import io.toro.trello.request.ActionMemberCreatorRequest
import io.toro.trello.response.ActionMemberCreatorFieldResponse
import io.toro.trello.request.ActionMemberCreatorFieldRequest
import io.toro.trello.response.ActionOrganizationResponse
import io.toro.trello.request.ActionOrganizationRequest
import io.toro.trello.response.ActionOrganizationFieldResponse
import io.toro.trello.request.ActionOrganizationFieldRequest
import io.toro.trello.request.ActionTextRequest
import io.toro.trello.request.ActionTextValueRequest
import io.toro.trello.response.ListResponse
import io.toro.trello.request.ListRequest
import io.toro.trello.request.ListUpdateRequest
import io.toro.trello.response.ListFieldResponse
import io.toro.trello.request.ListFieldRequest
import io.toro.trello.response.ListActionResponse
import io.toro.trello.request.ListActionRequest
import io.toro.trello.response.ListBoardResponse
import io.toro.trello.request.ListBoardRequest
import io.toro.trello.response.ListBoardFieldResponse
import io.toro.trello.request.ListBoardFieldRequest
import io.toro.trello.response.ListCardResponse
import io.toro.trello.request.ListCardRequest
import io.toro.trello.response.ListCardFilterResponse
import io.toro.trello.request.ListCardFilterRequest
import io.toro.trello.request.ListClosedRequest
import io.toro.trello.request.ListNameRequest
import io.toro.trello.request.ListPosRequest
import io.toro.trello.request.ListSubscribedRequest
import io.toro.trello.request.ListCreateRequest
import io.toro.trello.response.ListCreateResponse
import io.toro.trello.response.ListArchiveAllCardResponse
import io.toro.trello.request.ListCreateCardRequest
import io.toro.trello.response.ChecklistResponse
import io.toro.trello.request.ChecklistRequest
import io.toro.trello.response.ChecklistFieldResponse
import io.toro.trello.request.ChecklistFieldRequest
import io.toro.trello.response.ChecklistBoardResponse
import io.toro.trello.request.ChecklistBoardRequest
import io.toro.trello.response.ChecklistBoardFieldResponse
import io.toro.trello.request.ChecklistBoardFieldRequest
import io.toro.trello.response.ChecklistCardResponse
import io.toro.trello.request.ChecklistCardRequest
import io.toro.trello.response.ChecklistCardFilterResponse
import io.toro.trello.request.ChecklistCardFilterRequest
import io.toro.trello.response.ChecklistCheckItemResponse
import io.toro.trello.request.ChecklistCheckItemRequest
import io.toro.trello.request.ChecklistCheckItemByIdRequest
import io.toro.trello.request.ChecklistUpdateRequest
import io.toro.trello.request.ChecklistUpdateNameRequest
import io.toro.trello.request.ChecklistUpdatePosRequest
import io.toro.trello.request.ChecklistCreateRequest
import io.toro.trello.request.ChecklistCreateCheckItemRequest
import io.toro.trello.request.MemberRequest
import io.toro.trello.response.MemberResponse
import io.toro.trello.request.MemberFieldRequest
import io.toro.trello.response.MemberFieldResponse
import io.toro.trello.request.MemberActionRequest
import io.toro.trello.response.MemberActionResponse
import io.toro.trello.request.MemberBoardBackgroundByIdRequest
import io.toro.trello.response.MemberBoardStarResponse
import io.toro.trello.request.MemberBoardStarRequest
import io.toro.trello.request.MemberBoardBackgroundRequest
import io.toro.trello.response.MemberBoardBackgroundResponse
import io.toro.trello.request.MemberBoardRequest
import io.toro.trello.response.MemberBoardResponse
import io.toro.trello.request.MemberBoardFilterRequest
import io.toro.trello.response.MemberBoardInvitedResponse
import io.toro.trello.request.MemberBoardInvitedRequest
import io.toro.trello.response.MemberBoardInvitedFieldResponse
import io.toro.trello.request.MemberBoardInvitedFieldRequest

@CompileStatic
class TrelloConnector {
	private static ObjectMapper mapper

	static {
		// configure jackson and enable it to use Jackson and JAXB annotations on our beans
		if ( mapper == null ) {
			mapper = new ObjectMapper()
			mapper.setAnnotationIntrospector( AnnotationIntrospector.pair(
					new JacksonAnnotationIntrospector(),
					new JaxbAnnotationIntrospector( TypeFactory.defaultInstance() ) )
			)
		}
	}

	@TypeChecked( TypeCheckingMode.SKIP )
	private static <T, T1, T2> T contactTrello( String alias, String path, Method method, T1 reqBody, T2 query, Class<T> clazz ) throws TrelloException {
		String keyCredential = getCredential(alias)[0]
		String tokenCredential = getCredential(alias)[1]
		
		Map<String, ?> params = [ 
			path: path, 
			requestContentType: ContentType.JSON,
			query: [ 
				key: keyCredential ,
				token: tokenCredential 
			]
		]
		
		try {
			mapper.setSerializationInclusion( Include.NON_NULL )
			if ( reqBody ) {
				params << [ body: reqBody ]
			}

			if ( query ) {
				if ( query instanceof Map ){
					params.query << query
				}else{
					params.query << mapper.convertValue( query, Map )
				}
			}
			RESTClient rest = new RESTClient( 'https://api.trello.com/1/' )
			
			rest.encoder.putAt( 'application/json' ) { body, content ->
				StringEntity entity = new StringEntity( mapper.writeValueAsString( body ), Charset.defaultCharset() )
				entity.setContentType( content.toString() )
				System.out.println("entity - " + entity)
				entity
			}

			rest.parser.putAt( 'application/json' ) { resp ->
				HttpResponse response = resp as HttpResponse
				if ( ( response.statusLine.statusCode == 200 || response.statusLine.statusCode == 201 )
					&& clazz != null )
					mapper.readValue( new InputStreamReader( response.getEntity().getContent(),
						ParserRegistry.getCharset( response ) ), clazz )
				else
					new ParserRegistry().parseJSON( response )
			}

			HttpResponseDecorator response = rest."${method.toString().toLowerCase()}"( params ) as HttpResponseDecorator
			T restResponse = response.data as T
			restResponse
		} catch( HttpResponseException ex ) {
			throw new TrelloException( ex.response.data.text )
		}
	}

	private static contactTrello( String alias, String path, Method method, Object body ) {
		contactTrello( alias, path, method, body, null, null )
	}

	private static <T> T contactTrello( String alias, String path, Method method, Class<T> clazz ) {
		contactTrello( alias, path, method, null, clazz )
	}

	private static <T, R> T contactTrello( String alias, String path, Method method, R data, Class<T> clazz ) {
		if ( method == Method.GET || method == Method.DELETE )
			contactTrello( alias, path, method, null, data, clazz )
		else if ( method == Method.POST || method == Method.PUT )
			contactTrello( alias, path, method, data, null, clazz )
	}

	private static contactTrello( String alias, String path, Method method ) {
		contactTrello( alias, path, method, null, null )
	}

	/**
	 * MEMBER 58 - 57
	 */

	/**
	 * https://trello.com/1/members/[member_id or username]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @param idOrUsername the id or username of the member
	 * @param MemberRerquest consist of
	 * @return MemberResponse
	 */

	static MemberResponse trelloGetMember(String alias , String idOrUsername , MemberRequest request) {
		contactTrello(alias, "members/${idOrUsername}" , Method.GET , request , MemberResponse)
	}

	/**
	 * https://trello.com/1/members/[member_id or username]/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member field response
	 */

	static MemberFieldResponse trelloGetMemberField(String alias , String idOrUsername , MemberFieldRequest request) {
		String field = (request.field != null ? request.field : "")
		contactTrello(alias, "members/${idOrUsername}/${field}" , Method.GET , MemberFieldResponse)
	}

   /**
	* https://trello.com/1/members/[member_id or username]/actions
	* @param alias keyword you used to save your credentials in TOROProperties
	* @return Member action response
	*/

	static MemberActionResponse[] trelloGetMemberAction(String alias, String idOrUsername , MemberActionRequest request) {
		contactTrello(alias, "members/${idOrUsername}/actions" , Method.GET , request ,MemberActionResponse[])
	}

	/**
	 * https://trello.com/1/members/[member_id or username]/boardBackgrounds
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member board background response
	 */

	static MemberBoardBackgroundResponse[] trelloGetMemberBoardBackground(String alias , String idMemberOrUsername , MemberBoardBackgroundRequest request) {
		contactTrello(alias,"members/${idMemberOrUsername}/boardBackgrounds" , Method.GET, request , MemberBoardBackgroundResponse[])
	}

	/**
	 * https://trello.com/1/members/[member_id or username]/boardBackgrounds/[boardBackground_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member board background response
	 */
	static MemberBoardBackgroundResponse[] trelloGetMemberBoardBackgroundsById(String alias , String idMemberOrUsername , MemberBoardBackgroundByIdRequest request) {
		String id = (request.idBoardBackground != null ? request.idBoardBackground : "")
		contactTrello(alias,"members/${idMemberOrUsername}/boardBackgrounds/${id}" , Method.GET, request , MemberBoardBackgroundResponse[])
	}

	/**
	 * https://trello.com/1/members/[member_id or username]/boardStar
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member board star response
	 */

	static MemberBoardStarResponse[] trelloGetMemberBoardStar(String alias , String idMemberOrUsername) {
		contactTrello(alias,"members/${idMemberOrUsername}/boardStars" , Method.GET , MemberBoardStarResponse[])
	}

	/**
	 * https://trello.com/1/members/[member_id or username]/boardStar[boardStar_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member board star response
	 */
	static MemberBoardStarResponse trelloGetMemberBoardStarsById(String alias , String idMemberOrUsername , MemberBoardStarRequest request) {
		String id = (request.idBoardStar != null ? request.idBoardStar : "")
		contactTrello(alias,"members/${idMemberOrUsername}/boardBackgrounds/${id}" , Method.GET, request , MemberBoardStarResponse)
	}

	/**
	 * https://trello.com/1/members/[member_id or username]/boards
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member board response
	 */

	static MemberBoardResponse[] trelloGetMemberBoard(String alias ,String idMemberOrUsername, MemberBoardRequest request) {
		contactTrello(alias , "members/${idMemberOrUsername}/boards" , Method.GET , request , MemberBoardResponse[])
	}

	/**
	 * https://trello.com/1/members/[member_id or username]/boards/[filter]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member board filter response
	 */

	static MemberBoardResponse[] trelloGetMemberBoardFilter(String alias ,String idMemberOrUsername, MemberBoardFilterRequest request) {
		String filter = (request.filter != null ? request.filter : "")
		contactTrello(alias , "members/${idMemberOrUsername}/boards/${filter}" , Method.GET, request , MemberBoardResponse[])
	}

	/**
	 * https://trello.com/1/members/[member_id or username]/boardsInvited
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member board invited response
	 */
	static <T> T trelloGetMemberBoardInvited(String alias , String idMemberOrUsername , MemberBoardInvitedRequest request) {
		contactTrello(alias , "members/${idMemberOrUsername}/boardsInvited" , Method.GET, request , T)
	}

	/**
	 * https://trello.com/1/members/[member_id or username]/boardsInvited/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member board invited field response
	 */

	static <T> T trelloGetMemberBoardInvitedField(String alias , String idMemberOrUsername , MemberBoardInvitedFieldRequest request) {
		String field = (request.field != null ? request.field : "")
		contactTrello(alias , "members/${idMemberOrUsername}/boardsInvited/${field}" , Method.GET, request , T)
	}

	static <T> T trelloGetMemberCard(String alias , String idMember , BoardCardQuery query) {
		contactTrello(alias,"members/${idMember}/cards" , Method.GET, query , T)
	}

	static <T> T trelloGetMemberCardFilter(String alias , String idMember , String filter) {
		contactTrello(alias,"members/${idMember}/cards/${filter}" , Method.GET, [filter:filter] , T)
	}

	static <T> T trelloGetMemberCustomBoardBackgrounds(String alias , String idMember , String filter) {
		contactTrello(alias,"members/${idMember}/customBoardBackgrounds" , Method.GET, [filter:filter] , T)
	}

	static <T> T trelloGetMemberCustomBoardBackgroundsById(String alias , String idMember , idBoardBackground , String fields) {
		contactTrello(alias,"members/${idMember}/customBoardBackgrounds/${idBoardBackground}" , Method.GET, [fields:fields , idBoardBackground:idBoardBackground] , T)
	}

	static <T> T trelloGetMemberCustomEmoji(String alias , String idMember ,String idCustomEmoji , String fields) {
		contactTrello(alias,"members/${idMember}/customEmoji" , Method.GET, [fields:fields] , T)
	}

	static <T> T trelloGetMemberCustomEmojiById(String alias , String idMember ,String idCustomEmoji , String fields) {
		contactTrello(alias,"members/${idMember}/customEmoji/${idCustomEmoji}" , Method.GET, [idCustomEmoji:idCustomEmoji,fields:fields] , T)
	}

	static <T> T trelloGetMemberCustomStickers(String alias , String idMember , String fields) {
		contactTrello(alias,"members/${idMember}/customStickers" , Method.GET, [fields:fields] , T)
	}

	static <T> T trelloGetMemberCustomStickersById(String alias , String idMember ,String idCustomSticker , String fields) {
		contactTrello(alias,"members/${idMember}/customStickers/${idCustomSticker}" , Method.GET, [idCustomSticker:idCustomSticker,fields:fields] , T)
	}

	//deltas

	static MemberNotification[] trelloGetMemberNotification(String alias , String idMember , BoardActionQuery query) {
		contactTrello(alias , "members/${idMember}/notifications" , Method.GET, query , MemberNotification[])
	}

	/**
	 * https://trello.com/1/members/[member_id]/organizations
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member organization response
	 */

	static MemberOrganizationResponse[] trelloGetMemberOrganization(String alias , String idMember , MemberOrganizationRequest request) {
		contactTrello(alias , "members/${idMember}/organizations" , Method.GET, request , MemberOrganizationResponse[])
	}

	/**
	 * https://trello.com/1/members/[member_id]/organizations/[filter]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member organization filter response
	 */

	static MemberOrganizationFilterResponse[] trelloGetMemberOrganizationFilter(String alias , String idMember , MemberOrganizationFilterRequest request) {
		contactTrello(alias , "members/${idMember}/organizations/${request.filter}" , Method.GET, request , MemberOrganizationFilterResponse[])
	}

	/**
	 * https://trello.com/1/members/[member_id]/organizationsInvited
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member organizationInvited response
	 */

	static <T> T trelloGetMemberOrganizationInvited(String alias , String idMember , MemberOrganizationInvitedRequest request) {
		contactTrello(alias, "members/${idMember}/organizationsInvited" , Method.GET, request , T )
	}

	/**
	 * https://trello.com/1/members/[member_id]/organizationsInvited/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member organizationInvited field response
	 */

	static <T> T trelloGetMemberOrganizationInvitedField(String alias , String idMember , MemberOrganizationInvitedFieldRequest request) {
		contactTrello(alias, "members/${idMember}/organizationsInvited/${request.field}" , Method.GET, request , T )
	}

	/**
	 * https://trello.com/1/members/[member_id]/savedSearches
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member saveSearches response
	 */

	static MemberSavedSearchesResponse[] trelloGetMemberSaveSearches(String alias , String idMember) {
		contactTrello(alias, "members/${idMember}/savedSearches" , Method.GET, MemberSavedSearchesResponse[] )
	}

	/**
	 * https://trello.com/1/members/[member_id]/savedSearches/[idSavedSearch]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member saveSearches response
	 */

	static MemberSavedSearchesResponse trelloGetMemberSaveSearchesById(String alias , String idMember , MemberSavedSearchesRequest request) {
		contactTrello(alias, "members/${idMember}/savedSearches/${request.idSavedSearch}" , Method.GET, request ,MemberSavedSearchesResponse)
	}

	/**
	 * https://trello.com/1/members/[member_id]/tokens
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Member saveSearches response
	 */

	static <T> T trelloGetMemberToken(String alias , String idMember , MemberTokenRequest request) {
		contactTrello(alias, "members/${idMember}/tokens" , Method.GET, request ,T)
	}

	/**
	 * CHECKLIST  15 - 15
	 */

	/**
	 * https://trello.com/1/checklists/[checklist_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist response
	 */

	static ChecklistResponse trelloGetChecklist( String alias, String checklistId, ChecklistRequest request ) {
		contactTrello( alias, "checklists/${checklistId}", Method.GET, request, ChecklistResponse )
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist field response
	 */

	static ChecklistFieldResponse trelloGetChecklistField( String alias, String checklistId, ChecklistFieldRequest request) {
		String field = (request.field != null ? request.field : "")
		contactTrello( alias, "checklists/${checklistId}/${field}", Method.GET , request , ChecklistFieldResponse)
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]/board
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist board response
	 */
	static ChecklistBoardResponse trelloGetChecklistBoard( String alias, String checklistId, ChecklistBoardRequest request ) {
		contactTrello( alias, "checklists/${checklistId}/board", Method.GET , request , ChecklistBoardResponse)
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]/board/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist board field response
	 */

	static ChecklistBoardFieldResponse trelloGetChecklistBoardField(String alias, String checklistId, ChecklistBoardFieldRequest request) {
		String field = (request.field != null ? request.field : "")
		contactTrello( alias, "checklists/${checklistId}/board/${field}", Method.GET , request , ChecklistBoardFieldResponse)
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]/cards
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklistcard response
	 */

	static ChecklistCardResponse[] trelloGetChecklistCard( String alias, String checklistId, ChecklistCardRequest request) {
		contactTrello( alias, "checklists/${checklistId}/cards", Method.GET, request, ChecklistCardResponse[])
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]/cards/[filter]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklistcard filter response
	 */

	static ChecklistCardFilterResponse[] trelloGetCardFilterByChecklist(String alias, String checklistId, ChecklistCardFilterRequest request ) {
		String filter = (request.filter != null ? request.filter : "")
		contactTrello( alias, "checklists/${checklistId}/cards/${filter}", Method.GET , request  , ChecklistCardFilterResponse[])
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]/checkItems
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist checkItems response
	 */

	static ChecklistCheckItemResponse[] trelloGetChecklistCheckItem(String alias, String checklistId, ChecklistCheckItemRequest request) {
		contactTrello( alias, "checklists/${checklistId}/checkitems", Method.GET, request, ChecklistCheckItemResponse[] )
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]/checkItems/[checkitem_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist checkItems response
	 */

	static ChecklistCheckItemResponse trelloGetChecklistCheckItemById(String alias, String checklistId, ChecklistCheckItemByIdRequest request) {
		String idCheckItem = (request.idCheckItem != null ? request.idCheckItem : "")
		contactTrello(alias , "checklists/${checklistId}/checkitems/${idCheckItem}", Method.GET, request , ChecklistCheckItemResponse)
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist response
	 */

	static ChecklistResponse trelloUpdateChecklist(String alias, String checklistId , ChecklistUpdateRequest request) {
		contactTrello( alias, "checklists/${checklistId}", Method.PUT, request, ChecklistResponse )
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]/name
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist update name response
	 */

	static ChecklistResponse trelloUpdateChecklistName(String alias, String checklistId , ChecklistUpdateNameRequest request) {
		contactTrello( alias, "checklists/${checklistId}/name", Method.PUT, request , ChecklistResponse )
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]/pos
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist update pos response
	 */

	static ChecklistResponse trelloUpdateChecklistPos(String alias, String checklistId , ChecklistUpdatePosRequest request) {
		contactTrello( alias, "checklists/${checklistId}/pos", Method.PUT, request, ChecklistResponse )
	}

	/**
	 * https://trello.com/1/checklists
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist response
	 */

	static ChecklistResponse trelloCreateChecklist(String alias, String cardId,ChecklistCreateRequest request) {
		contactTrello( alias, "checklists", Method.POST, request, ChecklistResponse)
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]/checkitems
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist checkitem response
	 */

	static ChecklistCheckItemResponse trelloCreateCheckItem(String alias, String checklistId , ChecklistCreateCheckItemRequest request) {
		contactTrello( alias, "checklists/${checklistId}/checkitems", Method.POST, request, ChecklistCheckItemResponse)
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist response
	 */

	static <T> T trelloDeleteChecklist( String alias, String checklistId) {
		contactTrello( alias, "checklists/${checklistId}", Method.DELETE, T )
	}

	/**
	 * https://trello.com/1/checklists/[checklist_id]/checkitems/[checkitem_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Checklist checkitem response
	 */

	static <T> T trelloChecklistDeleteCheckItem( String alias, String checklistId , String checkItemId) {
		contactTrello( alias, "checklists/${checklistId}/checkitems/${checkItemId}", Method.DELETE, T )
	}

	/**
	 * LISTS 17 - 16
	 */

	/**
	 * https://trello.com/1/lists/[list_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List response
	 */

	static ListResponse trelloGetLists(String alias , String listId , ListRequest request) {
		contactTrello( alias, "lists/${listId}", Method.GET, request, ListResponse)
	}

	/**
	 * https://trello.com/1/lists/[list_id]/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List field response
	 */

	static ListFieldResponse trelloGetListField(String alias, String listId , ListFieldRequest request) {
		String field = (request.field != null ? request.field : "")
		contactTrello( alias, "lists/${listId}/${field}", Method.GET,  request , ListFieldResponse)
	}

	/**
	 * https://trello.com/1/lists/[list_id]/actions
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List action response
	 */

	static ListActionResponse[] trelloGetListAction(String alias, String listId , ListActionRequest request) {
		
		contactTrello( alias, "lists/${listId}/actions", Method.GET, request, ListActionResponse[])
	}

	/**
	 * https://trello.com/1/lists/[list_id]/board
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List board response
	 */

	static ListBoardResponse trelloGetListBoard(String alias, String listId , ListBoardRequest request) {
			
		contactTrello( alias, "lists/${listId}/board", Method.GET, request , ListBoardResponse)
	}

	/**
	 * https://trello.com/1/lists/[list_id]/board/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List board field response
	 */

	static ListBoardFieldResponse trelloGetListBoardList(String alias, String listId , ListBoardFieldRequest request) {
		
		String field = (request.field != null ? request.field : "")
		contactTrello( alias, "lists/${listId}/board/${field}", Method.GET, request , ListBoardFieldResponse)
	}

	/**
	 * https://trello.com/1/lists/[list_id]/cards
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List card response
	 */

	static ListCardResponse[] trelloGetListCard(String alias , String listId , ListCardRequest request) {
		contactTrello( alias, "lists/${listId}/cards", Method.GET, request, ListCardResponse[])
	}

	/**
	 * https://trello.com/1/lists/[list_id]/cards/[filter]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List card filter response
	 */

	static ListCardFilterResponse[] trelloGetListCardFilter(String alias , String listId , ListCardFilterRequest request) {
		String filter = (request.filter != null ? request.filter : "")
		contactTrello( alias, "lists/${listId}/cards/${filter}", Method.GET, request, ListCardFilterResponse[])
	}

	/**
	 * https://trello.com/1/lists/[list_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List response
	 */

	static ListResponse trelloUpdateList(String alias, String listId , ListUpdateRequest request) {
		contactTrello( alias, "lists/${listId}", Method.PUT, request, ListResponse)
	}

	/**
	 * https://trello.com/1/lists/[list_id]/closed
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List response
	 */

	static ListResponse trelloUpdateListClosed(String alias, String listId , ListClosedRequest request) {
		contactTrello( alias, "lists/${listId}/closed", Method.PUT, request, ListResponse)
	}

	/**
	 * https://trello.com/1/lists/[list_id]/idBoard
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List response
	 */

	static ListResponse trelloUpdateListIdBoard(String alias, String listId , ListIdBoardRequest request) {
		contactTrello( alias, "lists/${listId}/idBoard", Method.PUT, request , ListResponse)
	}

	/**
	 * https://trello.com/1/lists/[list_id]/name
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List response
	 */

	static ListResponse trelloUpdateListName(String alias, String listId , ListNameRequest request) {
		contactTrello( alias, "lists/${listId}/name", Method.PUT, request , ListResponse)
	}

	/**
	 * https://trello.com/1/lists/[list_id]/pos
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List response
	 */

	static ListResponse trelloUpdateListPos(String alias, String listId , ListPosRequest request) {
		contactTrello( alias, "lists/${listId}/pos", Method.PUT, request , ListResponse)
	}

	/**
	 * https://trello.com/1/lists/[list_id]/subscribed
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List response
	 */

	static ListResponse trelloUpdateListSubscribed(String alias, String listId ,ListSubscribedRequest request) {
		contactTrello( alias, "lists/${listId}/subscribed", Method.PUT, request , ListResponse)
	}

	/**
	 * https://trello.com/1/lists
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List create response
	 */

	static ListCreateResponse trelloCreateList(String alias , ListCreateRequest request) {
		contactTrello( alias, "lists", Method.POST, request, ListCreateResponse)
	}

	/**
	 * https://trello.com/1/lists/[list_id]/archiveAllCards
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List archiveAllCards response
	 */

	static ListArchiveAllCardResponse trelloCreateListArchiveAllCard(String alias , String listId) {
		contactTrello( alias, "lists/${listId}/archiveAllCards", Method.POST, ListArchiveAllCardResponse)
	}

	/**
	 * https://trello.com/1/lists/[list_id]/cards
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return List response
	 */

	static ListCardResponse trelloCreateListCard(String alias , String listId , ListCreateCardRequest request) {
		contactTrello( alias, "lists/${listId}/cards", Method.POST, request, ListCardResponse)
	}

	/**
	 * ACTIONS 19 - 19
	 */

	/**
	 * https://trello.com/1/actions/[action_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action response
	 */

	static ActionResponse trelloGetAction(String alias , String actionId , ActionRequest request) {
		contactTrello(alias,"actions/${actionId}",Method.GET,request , ActionResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action field response
	 */

	static ActionFieldResponse trelloGetActionField(String alias , String actionId , ActionFieldRequest request) {
		contactTrello(alias,"actions/${actionId}/${request.field}",Method.GET,request , ActionFieldResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/board
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action board response
	 */

	static ActionBoardResponse trelloGetActionBoard(String alias , String actionId , ActionBoardRequest request) {
		contactTrello(alias,"actions/${actionId}/board",Method.GET,request , ActionBoardResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/board/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action board field response
	 */

	static ActionBoardFieldResponse trelloGetActionBoardField(String alias , String actionId , ActionBoardFieldRequest request) {
		contactTrello(alias,"actions/${actionId}/board/${request.field}",Method.GET,request , ActionBoardFieldResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/card
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action card response
	 */

	static ActionCardResponse trelloGetActionCard(String alias , String actionId , ActionCardRequest request) {
		contactTrello(alias,"actions/${actionId}/card",Method.GET,request , ActionCardResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/card/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action card field response
	 */

	static ActionCardFieldResponse trelloGetActionCardField(String alias , String actionId , ActionCardFieldRequest request) {
		contactTrello(alias,"actions/${actionId}/card/${request.field}",Method.GET,request , ActionCardFieldResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/display
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action display response
	 */

	static ActionDisplayResponse trelloGetActionDisplay(String alias , String actionId) {
		contactTrello(alias,"actions/${actionId}/display",Method.GET , ActionDisplayResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/entities
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action entities response
	 */

	static ActionEntitiesResponse[] trelloGetActionEntities(String alias , String actionId) {
		contactTrello(alias,"actions/${actionId}/entities", Method.GET , ActionEntitiesResponse[])
	}

	/**
	 * https://trello.com/1/actions/[action_id]/list
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action list response
	 */

	static ActionListResponse trelloGetActionList(String alias , String actionId , ActionListRequest request) {
		contactTrello(alias,"actions/${actionId}/list", Method.GET ,request , ActionListResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/list/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action list field response
	 */

	static ActionListFieldResponse trelloGetActionListField(String alias , String actionId , ActionListFieldRequest request) {
		contactTrello(alias,"actions/${actionId}/list/${request.field}", Method.GET ,request , ActionListFieldResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/member
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action member response
	 */

	static ActionMemberResponse trelloGetActionMember(String alias , String actionId , ActionMemberRequest request) {
		contactTrello(alias,"actions/${actionId}/member", Method.GET ,request , ActionMemberResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/member/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action member field response
	 */

	static ActionMemberFieldResponse trelloGetActionMemberField(String alias , String actionId , ActionMemberFieldRequest request) {
		contactTrello(alias,"actions/${actionId}/member/${request.field}", Method.GET ,request , ActionMemberFieldResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/memberCreator
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action member creator response
	 */

	static ActionMemberCreatorResponse trelloGetActionMemberCreator(String alias , String actionId , ActionMemberCreatorRequest request) {
		contactTrello(alias,"actions/${actionId}/memberCreator", Method.GET ,request , ActionMemberCreatorResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/memberCreator/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action member creator field response
	 */

	static ActionMemberCreatorFieldResponse trelloGetActionMemberCreatorField(String alias , String actionId , ActionMemberCreatorFieldRequest request) {
		contactTrello(alias,"actions/${actionId}/memberCreator/${request.field}", Method.GET ,request , ActionMemberCreatorFieldResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/organization
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action organization response
	 */

	static ActionOrganizationResponse trelloGetActionOrganization(String alias , String actionId , ActionOrganizationRequest request) {
		contactTrello(alias,"actions/${actionId}/organization", Method.GET ,request , ActionOrganizationResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/organization/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action organization field response
	 */

	static ActionOrganizationFieldResponse trelloGetActionOrganizationField(String alias , String actionId , ActionOrganizationFieldRequest request) {
		contactTrello(alias,"actions/${actionId}/organization/${request.field}", Method.GET ,request , ActionOrganizationFieldResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action response
	 */

	static ActionResponse trelloUpdateAction(String alias , String actionId , ActionTextRequest request) {
		contactTrello(alias,"actions/${actionId}", Method.PUT ,request , ActionResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]/text
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action response
	 */

	static ActionResponse trelloUpdateActionText(String alias , String actionId , ActionTextValueRequest request) {
		contactTrello(alias,"actions/${actionId}/text", Method.PUT , request , ActionResponse)
	}

	/**
	 * https://trello.com/1/actions/[action_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Action response
	 */

	static <T> T trelloDeleteAction(String alias , String actionId) {
		contactTrello(alias,"actions/${actionId}", Method.DELETE , T)
	}


	/**
	 * BOARDS
	 */
	static Boards trelloGetBoard( String alias, String boardId, BoardQuery query ) {
		contactTrello( alias, "boards/${boardId}", Method.GET, query, Boards )
	}

	static <T> T trelloGetFieldsByBoard( String alias, String boardId, String field ) {
		Class<T> clazz
		switch( field ) {
			case 'memberships': clazz = Membership[]; break
			case 'labelNames': clazz = LabelNames; break
			case 'prefs': clazz = Prefs; break
			default: clazz = null
		}
		contactTrello( alias, "boards/${boardId}/${field}", Method.GET, clazz )
	}

	static <T> T trelloGetActionsByBoard( String alias, String boardId, BoardActionQuery query ) {
		Class<T> clazz = BoardAction[]
		if ( query.format.isEmpty() )
			query.format = 'list'
		else if ( query.format == 'count' || query.format == 'minimal')
			clazz = null
		contactTrello( alias, "boards/${boardId}/actions", Method.GET, query, clazz )
	}

	static BoardStars[] trelloGetBoardStars( String alias, String boardId, String filter ) {
		contactTrello( alias, "boards/${boardId}/boardStars", Method.GET, [ filter: filter ?: 'mine' ], BoardStars[] )
	}

	static BoardCard[] trelloGetBoardCards( String alias, String boardId, BoardCardQuery query ) {
		contactTrello( alias, "boards/${boardId}/cards", Method.GET, query, BoardCard[] )
	}

	static BoardCard[] trelloGetBoardCardByFilter( String alias, String boardId, String filter ) {
		contactTrello( alias, "boards/${boardId}/cards/${filter}", Method.GET, BoardCard[] )
	}

	static BoardCard trelloGetBoardCardById( String alias, String boardId, String cardId, CardQuery query ) {
		contactTrello( alias, "boards/${boardId}/cards/${cardId}", Method.GET, query, BoardCard )
	}

	static BoardChecklist[] trelloGetBoardChecklists( String alias, String boardId, BoardChecklistQuery query ) {
		contactTrello( alias, "boards/${boardId}/checklists", Method.GET, query, BoardChecklist[] )
	}

	//TODO
	static trelloGetBoardDelta( String alias, String boardId, String tag, String ixLastUpdate ) {
		contactTrello( alias, "boards/${boardId}/deltas", Method.GET, [ tags: tag, ixLastUpdate: ixLastUpdate ], null )
	}

	static Label[] trelloGetBoardLabels( String alias, String boardId, String fields, Integer limit ) {
		contactTrello( alias, "boards/${boardId}/labels", Method.GET, [ fields: fields ?: 'all',
			limit: limit ?: 50 ], Label[] )
	}

	static Label trelloGetBoardLabelById( String alias, String boardId, String labelId, String fields ) {
		contactTrello( alias, "boards/${boardId}/labels/${labelId}", Method.GET, [ fields: fields ?: 'all' ], Label )
	}

	static BoardList[] trelloGetBoardLists( String alias, String boardId, ListQuery query ) {
		contactTrello( alias, "boards/${boardId}/lists", Method.GET, query, BoardList[] )
	}

	static BoardList[] trelloGetBoardListsByFilter( String alias, String boardId, String filter ) {
		contactTrello( alias, "boards/${boardId}/lists/${filter}", Method.GET, BoardList[] )
	}

	static BoardMember[] trelloGetBoardMembers( String alias, String boardId, String filter, String fields,
		Boolean activity ) {
		contactTrello( alias, "boards/${boardId}/members", Method.GET, [ filter: filter ?: 'all',
			fields: fields ?: 'fullName,username', activity: activity ?: false ], BoardMember[] )
	}

	//TODO
	static trelloGetBoardMemberCards( String alias, String boardId, String memberId ) {
		contactTrello( alias, "boards/${boardId}/members/${memberId}/cards", Method.GET )
	}

	//TODO
	static BoardMember[] trelloGetBoardMembersInvited( String alias, String boardId, String fields ) {
		contactTrello( alias, "boards/${boardId}/membersInvited", Method.GET, [ fields: fields ?: 'all' ], BoardMember[] )
	}

	//TODO
	static BoardMember[] trelloGetBoardMembersInvitedByField( String alias, String boardId, String field ) {
		contactTrello( alias, "boards/${boardId}/membersInvited/${field}", Method.GET, BoardMember[] )
	}

	static BoardMember[] trelloGetBoardMembersByFilter( String alias, String boardId, String filter ) {
		contactTrello( alias, "boards/${boardId}/members/${filter}", Method.GET, BoardMember[] )
	}

	static Membership[] trelloGetBoardMemberships( String alias, String boardId, String filter, Boolean member,
		String memberFields ) {
		contactTrello( alias, "boards/${boardId}/memberships", Method.GET, [ filter: filter ?: 'all',
			member: member ?: false, member_fields: memberFields ?: 'fullName,username' ], Membership[] )
	}

	static Membership trelloGetBoardMembershipById( String alias, String boardId, String membershipId, Boolean member,
		String memberFields ) {
		contactTrello( alias, "boards/${boardId}/memberships/${membershipId}", Method.GET, [ member: member ?: false,
			member_fields: memberFields ?: 'fullName,username' ], Membership )
	}

	static BoardPrefs trelloGetBoardPrefs( String alias, String boardId ) {
		contactTrello( alias, "boards/${boardId}/myPrefs", Method.GET, BoardPrefs )
	}

	static Organization trelloGetBoardOrganization( String alias, String boardId, String fields ) {
		contactTrello( alias, "boards/${boardId}/organization", Method.GET, [ fields: fields ?: 'all' ], Organization )
	}

	static <T> T trelloGetBoardOrgByField( String alias, String boardId, String field ) {
		Class<T> clazz
		switch( field ) {
			case 'memberships': clazz = Membership[]; break
			case [ 'powerUps', 'products', 'invitations', 'premiumFeatures', 'idBoards' ]: clazz = String[]; break
			case 'prefs': clazz = OrgPrefs; break
			default: clazz = null
		}
		contactTrello( alias, "boards/${boardId}/organization/${field}", Method.GET, clazz )
	}

	static Boards trelloUpdateBoard( String alias, String boardId, UpdateBoard body ) {
		contactTrello( alias, "boards/${boardId}", Method.PUT, body, Boards )
	}

	//TODO
	static BoardMember trelloBoardAddMember( String alias, String boardId, String email, String fullName, String type ) {
		contactTrello( alias, "boards/${boardId}/members", Method.PUT, [ email: email, fullName: fullName,
			type: type ?: 'normal' ], BoardMember )
	}

	//TODO
	static BoardMember trelloBoardUpdateMember( String alias, String boardId, String memberId, String type ) {
		contactTrello( alias, "boards/${boardId}/members/${memberId}", Method.PUT, [ type: type ], BoardMember )
	}

	static Membership trelloBoardUpdateMembership( String alias, String boardId, String membershipId, String type,
		String memberFieldsQuery ) {
		contactTrello( alias, "boards/${boardId}/memberships/${membershipId}", Method.PUT, [ type: type ],
		[ member_fields: memberFieldsQuery ?: 'fullName,username' ], Membership )
	}

	static BoardPrefs trelloBoardUpdatePrefsEmailPos( String alias, String boardId, String value ) {
		contactTrello( alias, "boards/${boardId}/myPrefs/emailPosition", Method.PUT, [ value: value ], BoardPrefs )
	}

	static BoardPrefs trelloBoardUpdatePrefsEmailList( String alias, String boardId, String value ) {
		contactTrello( alias, "boards/${boardId}/myPrefs/idEmailList", Method.PUT, [ value: value ], BoardPrefs )
	}

	static BoardPrefs trelloBoardUpdatePrefsShowListGuide( String alias, String boardId, Boolean value ) {
		contactTrello( alias, "boards/${boardId}/myPrefs/showListGuide", Method.PUT, [ value: value ], BoardPrefs )
	}

	static BoardPrefs trelloBoardUpdatePrefsShowSidebar( String alias, String boardId, Boolean value ) {
		contactTrello( alias, "boards/${boardId}/myPrefs/showSidebar", Method.PUT, [ value: value ], BoardPrefs )
	}

	static BoardPrefs trelloBoardUpdatePrefsShowSidebarAct( String alias, String boardId, Boolean value ) {
		contactTrello( alias, "boards/${boardId}/myPrefs/showSidebarActivity", Method.PUT, [ value: value ],
			BoardPrefs )
	}

	static BoardPrefs trelloBoardUpdatePrefsShowSidebarBoardAct( String alias, String boardId, Boolean value ) {
		contactTrello( alias, "boards/${boardId}/myPrefs/showSidebarBoardActions", Method.PUT, [ value: value ],
			BoardPrefs )
	}

	static BoardPrefs trelloBoardUpdatePrefsShowSidebarMembers( String alias, String boardId, Boolean value ) {
		contactTrello( alias, "boards/${boardId}/myPrefs/showSidebarMembers", Method.PUT, [ value: value ],
			BoardPrefs )
	}

	static Boards trelloSubscribeBoard( String alias, String boardId, Boolean value ) {
		contactTrello( alias, "boards/${boardId}/subscribed", Method.PUT, [ value: value ], Boards )
	}

	static Boards trelloCloseBoard( String alias, String boardId, Boolean value ) {
		contactTrello( alias, "boards/${boardId}/closed", Method.PUT, [ value: value ], Boards )
	}

	static Boards trelloCreateBoard( String alias, AddBoard body ) {
		contactTrello( alias, 'boards', Method.POST, body, Boards )
	}

	static CalendarEmailKey trelloBoardGenerateCalendarKey( String alias, String boardId ) {
		contactTrello( alias, "boards/${boardId}/calendarKey/generate", Method.POST, CalendarEmailKey )
	}

	static BoardChecklist trelloBoardAddChecklist( String alias, String boardId, String name ) {
		contactTrello( alias, "boards/${boardId}/checklists", Method.POST, [ name: name ], BoardChecklist )
	}

	static CalendarEmailKey trelloBoardGenerateEmailKey( String alias, String boardId ) {
		contactTrello( alias, "boards/${boardId}/emailKey/generate", Method.POST, CalendarEmailKey )
	}

	static Label trelloBoardAddLabel( String alias, String boardId, String name, String color ) {
		contactTrello( alias, "boards/${boardId}/labels", Method.POST, [ name: name, color: color ], Label )
	}

	static BoardList trelloBoardAddList( String alias, String boardId, String name, String pos ) {
		contactTrello( alias, "boards/${boardId}/lists", Method.POST, [ name: name, pos: pos ], BoardList )
	}

	static void trelloBoardMarkAsViewed( String alias, String boardId ) {
		contactTrello( alias, "boards/${boardId}/markAsViewed", Method.POST )
	}

	static String[] trelloBoardAddPowerUp( String alias, String boardId, String value ) {
		contactTrello( alias, "boards/${boardId}/powerUps", Method.POST, [ value: value ], String[] )
	}
	static Boards trelloBoardDeleteMember( String alias, String boardId, String memberId ) {
		contactTrello( alias, "boards/${boardId}/members/${memberId}", Method.DELETE, Boards )
	}

	static String[] trelloBoardRemovePowerUp( String alias, String boardId, String powerUp ) {
		contactTrello( alias, "boards/${boardId}/powerUps/${powerUp}", Method.DELETE, String[] )
	}
	/**
	 * CARDS
	 */
	static Card trelloGetCard( String alias, String cardId, CardQuery query ) {
		contactTrello( alias, "cards/${cardId}", Method.GET, query, Card )
	}

	static <T> T trelloGetCardByFields( String alias, String cardId, String field ) {
		Class<T> clazz
		switch ( field ) {
			case 'badges': clazz = Badges; break
			case 'checkItemStates': case 'labels': clazz = String[]
			default: clazz = null
		}
		contactTrello( alias, "cards/${cardId}/${field}", Method.GET, clazz )
	}

	static <T> T trelloGetCardByAction( String alias, String cardId, BoardActionQuery query ) {
		Class<T> clazz = BoardAction[]
		if ( query.format.isEmpty() )
			query.format = 'list'
		else if ( query.format == 'count' || query.format == 'minimal')
			clazz = null
		contactTrello( alias, "cards/${cardId}/actions", Method.GET, query, clazz )
	}

	static Attachment[] trelloCardGetAttachments( String alias, String cardId, String fields ) {
		contactTrello( alias, "cards/${cardId}/attachments", Method.GET, [ fields: fields ?: 'all' ], Attachment[] )
	}

	static CardBoard trelloCardGetBoard( String alias, String cardId, String fields ) {
		contactTrello( alias, "cards/${cardId}/board", Method.GET, [ fields: fields ?: 'all' ], CardBoard )
	}

	//TODO
	static <T> T trelloCardGetBoardByField( String alias, String cardId, String field ) {
		Class<T> clazz = null
		contactTrello( alias, "cards/${cardId}/board/${field}", Method.GET, clazz )
	}

	static CheckItemState[] trelloCardGetCheckItemStates( String alias, String cardId, String fields ) {
		contactTrello( alias, "cards/${cardId}/checkItemStates", Method.GET, [ fields: fields ?: 'all' ],
			CheckItemState[] )
	}

	static BoardChecklist[] trelloCardGetChecklists( String alias, String cardId, BoardChecklistQuery query ) {
		contactTrello( alias, "cards/${cardId}/checklists", Method.GET, query, BoardChecklist[] )
	}

	static BoardList trelloCardGetList( String alias, String cardId, String field ) {
		contactTrello( alias, "cards/${cardId}/list", Method.GET, [ fields: field ?: 'all' ], BoardList )
	}

	static String trelloCardGetListbyField( String alias, String cardId, String field ) {
		Map<String, String> output = contactTrello( alias, "cards/${cardId}/list/${field}", Method.GET ) as Map
		output.'_value'
	}

	static BoardMember[] trelloCardGetMembers( String alias, String cardId, String fields ) {
		contactTrello( alias, "cards/${cardId}/members", Method.GET, [ fields: fields ?: 'all' ], BoardMember[] )
	}

	static BoardMember[] trelloCardGetMembersVoted( String alias, String cardId, String fields ) {
		contactTrello( alias, "cards/${cardId}/membersVoted", Method.GET, [ fields: fields ?: 'all' ], BoardMember[] )
	}

	static Stickers[] trelloCardGetStickers( String alias, String cardId, String fields ) {
		contactTrello( alias, "cards/${cardId}/stickers", Method.GET, [ fields: fields ?: 'all' ], Stickers[] )
	}

	static Stickers trelloCardGetSpecificSticker( String alias, String cardId, String stickerId, String fields ) {
		contactTrello( alias, "cards/${cardId}/stickers/${stickerId}", Method.GET, [ fields: fields ?: 'all' ],
			Stickers )
	}

	static Card trelloUpdateCard( String alias, String cardId, UpdateCard body ) {
		contactTrello( alias, "cards/${cardId}", Method.PUT, body, Card )
	}

	static BoardAction trelloCardUpdateComment( String alias, String cardId, String actionId, String text ) {
		contactTrello( alias, "cards/${cardId}/actions/${actionId}/comments", Method.PUT, [ text: text ], BoardAction )
	}

	static BoardCheckItems trelloCardUpdateCheckItem( String alias, String cardId, String checklistId,
		String checkItemId, String name, String pos, String state ) {
		Map<String, String> body = [ name: name ?: null, pos: pos ?: null, state: state ?: null ]
		contactTrello( alias, "cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}", Method.PUT,
			body, BoardCheckItems )
	}

	static BoardCheckItems trelloCardUpdatedCheckItemName( String alias, String cardId, String checklistId,
		String checkItemId, String name ) {
		contactTrello( alias, "cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}/name", Method.PUT, [ value: name ], BoardCheckItems )
	}

	static BoardCheckItems trelloCardUpdateCheckItemPos( String alias, String cardId, String checklistId,
		String checkItemId, String pos ) {
		contactTrello( alias, "cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}/pos", Method.PUT,
			[ value: pos ], BoardCheckItems )
	}

	static BoardCheckItems trelloCardUpdateCheckItemState( String alias, String cardId, String checklistId,
		String checkItemId, String state ) {
		contactTrello( alias, "cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}/state", Method.PUT,
			[ value: state ], BoardCheckItems )
	}

	static Card trelloCloseCard( String alias, String cardId, boolean value ) {
		contactTrello( alias, "cards/${cardId}/closed", Method.PUT, [ value: value ], Card )
	}

	static Card trelloCardUpdateDescription( String alias, String cardId, String description ) {
		contactTrello( alias, "cards/${cardId}/desc", Method.PUT, [ value: description ], Card )
	}

	static Card trelloCardUpdateDueDate( String alias, String cardId, String due ) {
		contactTrello( alias, "cards/${cardId}/due", Method.PUT, [ due: due ?: null ], Card )
	}

	//TODO
	static Card trelloCardUpdateAttachmentCover( String alias, String cardId, String attachmentId ) {
		contactTrello( alias, "cards/${cardId}/idAttachmentCover", Method.PUT, [ value: attachmentId ?: null ], Card )
	}

	static Card trelloMoveCardToSpecificBoard( String alias, String cardId, String boardId, String listId ) {
		contactTrello( alias, "cards/${cardId}/idBoard", Method.PUT, [ value: boardId, idList: listId ?: null ],
			Card )
	}

	static Card trelloMoveCardToSpecificList( String alias, String cardId, String listId ) {
		contactTrello( alias, "cards/${cardId}/idList", Method.PUT, [ value: listId ], Card )
	}

	static Card trelloCardAddMembers( String alias, String cardId, List<String> memberId ) {
		contactTrello( alias, "cards/${cardId}/idMembers", Method.PUT, [ value: memberId.join( ',' ) ], Card )
	}

	static Card trelloCardUpdateLabel( String alias, String cardId, String label ) {
		contactTrello( alias, "cards/${cardId}/labels", Method.PUT, [ value: label ], Card )
	}

	static Card trelloCardUpdateName( String alias, String cardId, String name ) {
		contactTrello( alias, "cards/${cardId}/name", Method.PUT, [ value: name ], Card )
	}

	static Card trelloCardUpdatePos( String alias, String cardId, String pos ) {
		contactTrello( alias, "cards/${cardId}/pos", Method.PUT, [ value: pos ], Card )
	}

	static Stickers trelloCardUpdateSticker( String alias, String cardId, String stickerId, Integer top, Integer left,
		Integer zIndex, int rotate ) {
		contactTrello( alias, "cards/${cardId}/stickers/${stickerId}", Method.PUT, [ top: top, left: left,
			zIndex: zIndex, rotate: rotate ], Stickers )
	}

	static Card trelloSubscribeCard( String alias, String cardId, boolean value ) {
		contactTrello( alias, "cards/${cardId}/subscribed", Method.PUT, [ value: value ], Card )
	}

	static Card trelloCreateCard( String alias, AddCard body ) {
		contactTrello( alias, 'cards', Method.POST, body, Card )
	}

	/**
	 * LABEL 8 - 8
	 */

	static Labels trelloGetLabel(String alias , String labelId , LabelQuery query) {
		contactTrello( alias, "labels/${labelId}", Method.GET, query, Labels )
	}

	static LabelBoard trelloGetLabelBoard(String alias , String labelId , String fieldValue) {
		if(fieldValue.trim().equals(""))
			contactTrello( alias, "labels/${labelId}/board", Method.GET, [:], LabelBoard )
		else
			contactTrello( alias, "labels/${labelId}/board", Method.GET, [fields: fieldValue], LabelBoard )
	}

	static <T> T trelloGetLabelBoardField(String alias, String labelId , String fieldName) {
		contactTrello( alias, "labels/${labelId}/board/${fieldName}", Method.GET, T )
	}

	static Labels trelloUpdateLabel(String alias , String labelId , LabelQuery query) {
		contactTrello( alias, "labels/${labelId}", Method.PUT, query, Labels )
	}

	static Labels trelloUpdateLabelColor(String alias , String labelId , String color) {
		contactTrello( alias, "labels/${labelId}/color", Method.PUT, [value: color], Labels )
	}

	static Labels trelloUpdateLabelName(String alias , String labelId , String name) {
		contactTrello( alias, "labels/${labelId}/name", Method.PUT, [value: name], Labels )
	}

	static Labels trelloCreateLabel(String alias, String name , String color , String idBoard) {
		LabelQuery query = new LabelQuery(name : name, color: color , idBoard : idBoard)
		contactTrello( alias, "labels", Method.POST, query, Labels )
	}

	static <T> T trelloDeleteLabel( String alias, labelId) {
		contactTrello( alias, "labels/${labelId}", Method.DELETE, T )
	}

	/**
	 * NOTIFICATION 19 - 10
	 */

	/**
	 * https://trello.com/1/notifications/[notification_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Notification response
	 */

	static NotificationResponse trelloGetNotification(String alias , String idNotification , BoardActionQuery query) {
		contactTrello(alias , "notifications/${idNotification}" , Method.GET, query , NotificationResponse )
	}

	/**
	 * https://trello.com/1/notifications/[notification_id]/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Notification Field response
	 */

	static NotificationFieldResponse trelloGetNotificationField(String alias ,String idNotification , NotificationFieldRequest request ) {
		contactTrello(alias , "notifications/${idNotification}/${request.field}" , Method.GET, request , NotificationFieldResponse )
	}

	/**
	 * https://trello.com/1/notifications/[notification_id]/entities
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Notification Entities response
	 */

	static NotificationEntitiesResponse[] trelloGetNotificationEntities(String alias ,String idNotification) {
		contactTrello(alias , "notifications/${idNotification}/entities" , Method.GET, NotificationEntitiesResponse[] )
	}

	/**
	 * https://trello.com/1/notifications/[notification_id]/memberCreator
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Notification MemberCreator response
	 */

	static NotificationMemberCreatorResponse trelloGetNotificationMemberCreator(String alias ,String idNotification , NotificationMemberCreatorRequest request) {
		contactTrello(alias , "notifications/${idNotification}/memberCreator" , Method.GET, request , NotificationMemberCreatorResponse )
	}

	/**
	 * https://trello.com/1/notifications/[notification_id]/memberCreator/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Notification MemberCreator field response
	 */

	static NotificationMemberCreatorFieldResponse trelloGetNotificationMemberCreatorField(String alias ,String idNotification , NotificationMemberCreatorFieldRequest request) {
		contactTrello(alias , "notifications/${idNotification}/memberCreator/${request.field}" , Method.GET, request , NotificationMemberCreatorFieldResponse )
	}

	/**
	 * https://trello.com/1/notifications/[notification_id]/organization
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Notification Organization response
	 */

	static NotificationOrganizationResponse trelloGetNotificationOrganization(String alias ,String idNotification , NotificationOrganizationRequest request) {
		contactTrello(alias , "notifications/${idNotification}/organization" , Method.GET, request , NotificationOrganizationResponse )
	}

	/**
	 * https://trello.com/1/notifications/[notification_id]/organization/[field]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Notification Organization response
	 */

	static NotificationOrganizationFieldResponse trelloGetNotificationOrganizationField(String alias ,String idNotification , NotificationOrganizationFieldRequest request) {
		contactTrello(alias , "notifications/${idNotification}/organization/${request.field}" , Method.GET, request , NotificationOrganizationFieldResponse )
	}

	/**
	 * https://trello.com/1/notifications/[notification_id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Notification response
	 */

	static NotificationResponse trelloUpdateNotification(String alias ,String idNotification ,NotificationRequest request) {
		contactTrello(alias , "notifications/${idNotification}" , Method.PUT, request , NotificationResponse )
	}

	/**
	 * https://trello.com/1/notifications/[notification_id]/unread
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Notification response
	 */

	static NotificationResponse trelloUpdateNotificationFieldUnread(String alias ,String idNotification , NotificationFieldUnreadRequest request) {
		contactTrello(alias , "notifications/${idNotification}/unread" , Method.PUT, request , NotificationResponse )
	}

	/**
	 * https://trello.com/1/notifications/all/read
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Notification response
	 */

	static NotificationResponse trelloCreateNotification(String alias) {
		contactTrello(alias , "notifications/all/read" , Method.POST , NotificationResponse )
	}

	/**
	 * SEARCH 2 - 2
	 */

	/**
	 * https://trello.com/1/search
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Search response
	 */

	static SearchResponse trelloGetSearch(String alias , SearchRequest request) {
		contactTrello(alias, "search" , Method.GET , request , SearchResponse)
	}

	/**
	 * https://trello.com/1/search/members
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Search members response
	 */

	static SearchMemberResponse[] trelloGetSearchMember(String alias , SearchMemberRequest request) {
		contactTrello(alias, "search/members" , Method.GET , request , SearchMemberResponse[])
	}

	/**
	 * TYPES 1 - 1
	 */

	/**
	 * https://trello.com/1/types/[id]
	 * @param alias keyword you used to save your credentials in TOROProperties
	 * @return Search type response
	 */

	static TypeResponse trelloGetType(String alias , TypeRequest request) {
		contactTrello(alias , "types/${request.id}" , Method.GET , TypeResponse )
	}

	/**
	 * TOKENS 10 - 10
	 */

	static Token trelloGetToken(String alias , String token){
		contactTrello(alias , "tokens/${token}", Method.GET, Token)
	}

	static <T> T trelloGetTokenField(String alias , String token , String field) {
		contactTrello(alias , "tokens/${token}/${field}" , Method.GET , T)
	}

	static TokenMember trelloGetTokenMember(String alias , String token , String fields) {
		contactTrello(alias , "tokens/${token}/member" , Method.GET , [fields: fields] ,  TokenMember)
	}

	static <T> T trelloGetTokenMemberField(String alias , String token , String fields) {
		contactTrello(alias , "tokens/${token}/member/${fields}" , Method.GET ,  T)
	}

	static TokenWebhook[] trelloGetTokenWebhook(String alias , String token) {
		contactTrello(alias , "token/${token}/webhooks" , Method.GET, TokenWebhook[])
	}

	static TokenWebhook trelloGetTokenWebhookById(String alias , String token , webhookId) {
		contactTrello(alias , "token/${token}/webhooks/${webhookId}" , Method.GET, TokenWebhook)
	}

	static <T> T trelloUpdateTokenWebhook(String alias , String token , TokenWebhookRequest request) {
		contactTrello(alias , "token/${token}/webhooks" , Method.PUT , request , T)
	}

	static TokenWebhook trelloCreateTokenWebhook(String alias , String token , TokenWebhookRequest request) {
		contactTrello(alias , "token/${token}/webhooks" , Method.POST , request , TokenWebhook)
	}

	static <T> T trelloDeleteTokenWebhook(String alias , String token , TokenWebhookRequest request) {
		contactTrello(alias , "token/${token}/webhooks" , Method.DELETE , request , T)
	}

	static <T> T trelloDeleteTokenWebhookById(String alias , String token , webhookId) {
		contactTrello(alias , "token/${token}/webhooks/${webhookId}" , Method.DELETE, T)
	}

	/**
	 * WEBHOOK 8 - 8
	 * 
	 * 
	 */

	static <T> T trelloGetWebhookField(String alias , String webhookId , String field) {
		contactTrello(alias , "webhooks/${webhookId}/${field}", Method.GET, T)
	}

	static <T> T trelloUpdateWebhook(String alias , String webhookId , TokenWebhookRequest request) {		
		contactTrello(alias ,"webhooks/${webhookId}", Method.PUT , request , T)
	}

	static Webhook trelloUpdateWebhookActive(String alias , String webhookId , boolean value) {
		contactTrello(alias ,"webhooks/${webhookId}/active", Method.PUT , [value: value] ,Webhook)
	}

	static Webhook trelloUpdateWebhookCallbackURL(String alias , String webhookId , String value) {
		contactTrello(alias ,"webhooks/${webhookId}/callbackURL", Method.PUT , [value: value] ,Webhook)
	}

	static Webhook trelloUpdateWebhookDescription(String alias , String webhookId , String value) {
		contactTrello(alias ,"webhooks/${webhookId}/description", Method.PUT , [value: value] ,Webhook)
	}

	static Webhook trelloUpdateWebhookIdModel(String alias , String webhookId , String value) {
		contactTrello(alias ,"webhooks/${webhookId}/idModel", Method.PUT , [value: value] ,Webhook)
	}

	static Webhook trelloCreateWebhook(String alias , String token , TokenWebhookRequest request) {
		contactTrello(alias , "webhooks" , Method.POST , request , Webhook)
	}

	static <T> T trelloDeleteWebhookById(String alias , String idWebhook) {
		contactTrello(alias,"webhooks/${idWebhook}", Method.DELETE , T)
	}

	/**
	 * ORGANIZATIONS 38 - 32
	 */

	static Organizations trelloGetOrganization(String alias , String organizationIdOrName , OrganizationQuery query) {
		contactTrello(alias , "organizations/${organizationIdOrName}" , Method.GET , query , Organizations)
	}

	static <T> T trelloGetOrganizationField(String alias, String organizationIdOrName, String fieldValue) {
		contactTrello(alias, "organizations/${organizationIdOrName}/${fieldValue}" , Method.GET , T)
	}

	static OrganizationActions[] trelloGetOrganizationActions(String alias , String organizationIdOrName , RequestOrganizationActions request) {
		contactTrello(alias , "organizations/${organizationIdOrName}/actions" , Method.GET, request, OrganizationActions[])
	}

	static OrganizationBoards[] trelloGetOrganizationBoards(String alias , String organizationIdOrName , OrganizationBoardRequest request) {
		contactTrello(alias , "organizations/${organizationIdOrName}/boards" , Method.GET, request, OrganizationBoards[])
	}

	static <T> T trelloGetOrganizationBoardFilter(String alias , String organizationIdOrName , String filter) {
		contactTrello(alias , "organization/${organizationIdOrName}/boards/${filter}" , Method.GET, T)
	}

	static <T> T[] trelloGetOrganizationMembers(String alias, String organizationIdOrName) {
		contactTrello(alias , "organization/${organizationIdOrName}/members" , Method.GET, T[])
	}

	static <T> T[] trelloGetOrganizationMembersByFilter(String alias, String organizationIdOrName , String filter) {
		contactTrello(alias , "organization/${organizationIdOrName}/members/${filter}" , Method.GET, T[])
	}

	static <T> T[] trelloGetOrganizationMemberships(String alias, String organizationIdOrName) {
		contactTrello(alias , "organization/${organizationIdOrName}/memberships" , Method.GET, T[])
	}

	static <T> T trelloGetOrganizationMembershipById(String alias, String organizationIdOrName , String membershipId) {
		contactTrello(alias , "organization/${organizationIdOrName}/memberships/${membershipId}" , Method.GET, T)
	}

	static Organizations trelloUpdateOrganizationDesc(String alias , String organizationIdOrName , String desc) {
		contactTrello(alias, "organization/${organizationIdOrName}/desc" , Method.PUT, [value: desc] , Organizations)
	}

	static Organizations trelloUpdateOrganizationDisplayName(String alias , String organizationIdOrName , String displayName) {
		contactTrello(alias, "organization/${organizationIdOrName}/displayName" , Method.PUT, [value: displayName] , Organizations)
	}

	static Organizations trelloUpdateOrganizationMember(String alias , String organizationIdOrName , String email , String fullName , String type) {
		contactTrello(alias, "organization/${organizationIdOrName}/members" , Method.PUT, [email: email,fullName: fullName , type: type] , Organizations)
	}

	static Organizations trelloUpdateOrganizationMemberType(String alias , String organizationIdOrName, String memberId , String type) {
		contactTrello(alias, "organization/${organizationIdOrName}/members/${memberId}" , Method.PUT, [idMember: memberId , type: type] , Organizations)
	}

	static Organizations trelloUpdateOrganizationDeactivated(String alias , String organizationIdOrName, String memberId , boolean type) {
		contactTrello(alias, "organization/${organizationIdOrName}/members/${memberId}/deactivated" , Method.PUT, [idMember: memberId , type: type] , Organizations)
	}

	static <T> T trelloUpdateOrganizationMembership(String alias , String organizationIdOrName , String membershipId , String type , String memberFields) {
		contactTrello(alias,"organization/${organizationIdOrName}/memberships/${membershipId}" , Method.PUT ,[idMembership: membershipId , type: type , member_fields: memberFields], T)
	}

	static Organizations trelloUpdateOrganizationName(String alias , String organizationIdOrName , String name) {
		contactTrello(alias, "organization/${organizationIdOrName}/name" , Method.PUT, [value: name] , Organizations)
	}

	static <T> T trelloUpdateOrganizationAssociatedDomain(String alias , String organizationIdOrName , String domain) {
		contactTrello(alias, "organization/${organizationIdOrName}/prefs/associatedDomain" , Method.PUT, [value: domain] , T)
	}

	static <T> T trelloUpdateOrganizationBoardVisibilityRestrict(String alias , String organizationIdOrName , String value) {
		contactTrello(alias, "organization/${organizationIdOrName}/prefs/boardVisibilityRestrict/org" , Method.PUT, [value: value] , T)
	}

	static <T> T trelloUpdateOrganizationBoardVisibilityRestrictOrg(String alias , String organizationIdOrName , String value) {
		contactTrello(alias, "organization/${organizationIdOrName}/prefs/boardVisibilityRestrict/org" , Method.PUT, [value:value] , T)
	}

	static <T> T trelloUpdateOrganizationBoardVisibilityRestrictPrivate(String alias , String organizationIdOrName , String value) {
		contactTrello(alias, "organization/${organizationIdOrName}/prefs/boardVisibilityRestrict/private" , Method.PUT, [value: value] , T)
	}

	static <T> T trelloUpdateOrganizationBoardVisibilityRestrictPublic(String alias , String organizationIdOrName , String value) {
		contactTrello(alias, "organization/${organizationIdOrName}/prefs/boardVisibilityRestrict/public" , Method.PUT, [value: value] , T)
	}

	static <T> T trelloUpdateOrganizationExternalMembersDisabled(String alias , String organizationIdOrName , boolean value) {
		contactTrello(alias, "organization/${organizationIdOrName}/prefs/externalMembersDisabled" , Method.PUT, [value: value] , T)
	}

	static <T> T trelloUpdateOrganizationGoogleAppsVersion(String alias , String organizationIdOrName , int value) {
		contactTrello(alias, "organization/${organizationIdOrName}/prefs/googleAppsVersion" , Method.PUT, [value: value] , T)
	}

	static <T> T trelloUpdateOrganizationOrgInviteRestrict(String alias , String organizationIdOrName , String value) {
		contactTrello(alias, "organization/${organizationIdOrName}/prefs/orgInviteRestrict" , Method.PUT, [value: value] , T)
	}

	static <T> T trelloUpdateOrganizationPermissionLevel(String alias , String organizationIdOrName , String value) {
		contactTrello(alias, "organization/${organizationIdOrName}/prefs/permissionLevel" , Method.PUT, [value: value] , T)
	}

	static <T> T trelloUpdateOrganizationWebsite(String alias , String organizationIdOrName , String value) {
		contactTrello(alias, "organization/${organizationIdOrName}/website", Method.PUT, [value: value], T)
	}

	static <T> T trelloDeleteOrganization(String alias , String organizationIdOrName) {
		contactTrello(alias, "organization/${organizationIdOrName}" , Method.DELETE, T)
	}

	static <T> T trelloDeleteOrganizationLogo(String alias , String organizationIdOrName) {
		contactTrello(alias, "organization/${organizationIdOrName}/logo" , Method.DELETE, T)
	}

	static <T> T trelloDeleteOrganizationMember(String alias , String organizationIdOrName , String idMember) {
		contactTrello(alias, "organization/${organizationIdOrName}/members/${idMember}", Method.DELETE, [idMember: idMember], T)
	}

	static <T> T trelloDeleteOrganizationMemberAll(String alias , String organizationIdOrName , String idMember) {
		contactTrello(alias, "organization/${organizationIdOrName}/members/${idMember}/all", Method.DELETE, [idMember: idMember], T)
	}

	static <T> T trelloDeleteOrganizationAssociatedDomain(String alias , String organizationIdOrName) {
		contactTrello(alias, "organization/${organizationIdOrName}/prefs/associatedDomain" , Method.DELETE, T)
	}

	static <T> T trelloDeleteOrganizationOrgInviteRestrict(String alias , String organizationIdOrName) {
		contactTrello(alias, "organization/${organizationIdOrName}/prefs/orgInviteRestrict" , Method.DELETE , T)
	}

	static String[] getCredential(String alias) {
		try {
			return alias.split(':')
		} catch (Exception e) {
			return null
			e.printStackTrace()
		}
	}
}
