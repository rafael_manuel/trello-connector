package io.toro.trello.exception

/**
 * Trello exception handling class
 * @author charlesturla
 *
 */
class TrelloException extends Exception {

	TrelloException( String message ) {
		super( message )
	}

	TrelloException( Throwable t ) {
		super( t )
	}
}
