package io.toro.trello.model.actions

import groovy.transform.ToString

/**
 * Trello action entitied model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionEntities {

	String type
	String id
	String shortLink
	String text
	String username

}
