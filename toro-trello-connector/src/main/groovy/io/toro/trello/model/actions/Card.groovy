package io.toro.trello.model.actions

import groovy.transform.ToString

/**
 * Trello card model for entities model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class Card {

	String type
	String id
	String shortLink
	String text

}
