package io.toro.trello.model.actions

import groovy.transform.ToString
import io.toro.trello.model.boards.Boards
import io.toro.trello.model.cards.Card
import io.toro.trello.model.lists.Lists

/**
 * Trello data model for action response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class Data {

	Boards board
	Lists list
	Card card

}
