package io.toro.trello.model.actions

import groovy.transform.ToString

/**
 * Trello entities model for action display response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class Entities {

	Card card
	List list
	MemberCreator memberCreator

}
