package io.toro.trello.model.actions

import groovy.transform.ToString

/**
 * Trello list model for entities model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class List {

	String type
	String id
	String text

}
