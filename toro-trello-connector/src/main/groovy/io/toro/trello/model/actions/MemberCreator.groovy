package io.toro.trello.model.actions

import groovy.transform.ToString

/**
 * Trello memberCreator model for entities model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberCreator {

	String type
	String id
	String username
	String text

}
