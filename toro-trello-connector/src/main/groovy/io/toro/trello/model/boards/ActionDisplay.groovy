package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Board action display entity model
 * @author charlesturla
 *
 */

@ToString( includeNames = true )
class ActionDisplay {

	String translationKey
	DisplayEntities entities

}
