package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Board action entities model
 * @author charlesturla
 *
 */

@ToString( includeNames = true, ignoreNulls = true )
class ActionEntities {

	String type
	Boolean hideIfContext
	String id
	String username
	String text
	String idContext
	String shortLink
	Integer pos

}
