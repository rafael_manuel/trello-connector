package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Add a new board
 * @author charlesturla
 *
 */

@ToString( includeNames = true, includeSuper = true )
class AddBoard extends BasicBoard {

	String idBoardSource
	String keepFromSource
	String powerUps

}
