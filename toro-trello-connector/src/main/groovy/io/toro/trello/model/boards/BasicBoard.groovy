package io.toro.trello.model.boards

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlElements

/**
 * Trello board basic entity for adding and updating
 * @author charlesturla
 *
 */

@ToString( includeNames = true )
class BasicBoard {

	String name
	String desc
	String idOrganization
	@XmlElements( [
		@XmlElement( name = 'prefs_permissionLevel' ),
		@XmlElement( name = 'prefs/permissionLevel' )
	] )
	String prefsPermissionLevel
	@XmlElements( [
		@XmlElement( name = 'prefs_voting' ),
		@XmlElement( name = 'prefs/voting' )
		] )
	String prefsVoting
	@XmlElements( [
		@XmlElement( name = 'prefs_comments' ),
		@XmlElement( name = 'prefs/comments' )
		] )
	String prefsComment
	@XmlElements( [
		@XmlElement( name = 'prefs_invitations' ),
		@XmlElement( name = 'prefs/invitations' )
		] )
	String prefsInvitations
	@XmlElements( [
		@XmlElement( name = 'prefs_selfJoin' ),
		@XmlElement( name = 'prefs/selfJoin' )
		] )
	String prefsSelfjoin
	@XmlElements( [
		@XmlElement( name = 'prefs_cardCovers' ),
		@XmlElement( name = 'prefs/cardCovers' )
	] )
	String prefsCardCovers
	@XmlElements( [
		@XmlElement( name = 'prefs_background' ),
		@XmlElement( name = 'prefs/background' )
	] )
	String prefsBackground
	@XmlElements( [
		@XmlElement( name = 'prefs_cardAging' ),
		@XmlElement( name = 'prefs/cardAging' )
	] )
	String prefsCardAging

}
