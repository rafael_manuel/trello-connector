package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Board action entity model
 * @author charlesturla
 *
 */

@ToString( includeNames = true, ignoreNulls = true )
class BoardAction {

	String id
	String idMemberCreator
	Map<String, ?> data
	String type
	Date date
	BoardMember member
	BoardMember memberCreator
	List<ActionEntities> entities
	ActionDisplay display

}

