package io.toro.trello.model.boards

import groovy.transform.ToString
import io.toro.trello.model.cards.Card

/**
 * Board card list model
 * @author charlesturla
 *
 */

@ToString( includeNames = true, includeSuper = true, ignoreNulls = true )
class BoardCard extends Card {

	List<?> checklists

}
