package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Board check items model
 * @author charlesturla
 *
 */

@ToString( includeNames = true )
class BoardCheckItems {

	String state
	String id
	String name
	DescData nameData
	String type
	Integer pos

}
