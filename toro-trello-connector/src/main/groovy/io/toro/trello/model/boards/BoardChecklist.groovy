package io.toro.trello.model.boards

import groovy.transform.ToString
import io.toro.trello.model.cards.Card

/**
 * Board checklists entity model
 * @author charlesturla
 *
 */

@ToString( includeNames = true, ignoreNulls = true )
class BoardChecklist {

	String id
	String name
	String idCard
	String idBoard
	Integer pos
	List<BoardCheckItems> checkItems
	List<Card> cards

}
