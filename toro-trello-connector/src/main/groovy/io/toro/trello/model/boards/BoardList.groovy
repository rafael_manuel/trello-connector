package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Board list data
 * @author charlesturla
 *
 */

@ToString( includeNames = true, ignoreNulls = true )
class BoardList {

	String name
	String id
	String idBoard
	Integer pos
	Boolean closed
	Boolean subscribed

}
