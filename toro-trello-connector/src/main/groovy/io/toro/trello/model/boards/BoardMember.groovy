package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Board member entity model
 * @author charlesturla
 *
 */

@ToString( includeNames = true, ignoreNulls = true )
class BoardMember {

	String id
	String avatarHash
	String fullName
	String initials
	String text
	String bio
	DescData bioData
	Boolean confirmed
	List<?> idPremOrgsAdmin
	String memberType
	List<?> products
	String status
	String url
	String username
}
