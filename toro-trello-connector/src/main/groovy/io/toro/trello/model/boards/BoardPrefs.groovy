package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Board prefs entity
 * @author charlesturla
 *
 */

@ToString( includeNames = true, ignoreNulls = true )
class BoardPrefs {
	Boolean showSidebar
	Boolean showSidebarMembers
	Boolean showSidebarBoardActions
	Boolean showSidebarActivity
	Boolean showListGuide
	String emailKey
	Object idEmailList
	String emailPosition
	String calendarKey
	String fullEmail
}
