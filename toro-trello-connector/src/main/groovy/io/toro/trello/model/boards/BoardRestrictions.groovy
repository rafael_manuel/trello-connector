package io.toro.trello.model.boards

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello permission levels
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class BoardRestrictions {
	@XmlElement( name = 'private' )
	String privateLevel
	String org
	@XmlElement( name = 'public' )
	String publicLevel
}
