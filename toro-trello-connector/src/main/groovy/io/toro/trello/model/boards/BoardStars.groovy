package io.toro.trello.model.boards

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Board stars entity model
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class BoardStars {
	@XmlElement( name = '_id' )
	String id
	String idBoard
	Integer pos
}
