package io.toro.trello.model.boards

import groovy.transform.ToString
import io.toro.trello.model.fields.LabelNames
import io.toro.trello.model.fields.Prefs

/**
 * Trello boards entity model
 * @author charlesturla
 *
 */
@ToString( includeNames = true, ignoreNulls = true )
class Boards {
	String id
	String name
	String desc
	DescData descData
	Boolean closed
	String idOrganization
	Boolean pinned
	String url
	String shortUrl
	String shortLink
	Prefs prefs
	LabelNames labelNames
	List<BoardMember> members
}
