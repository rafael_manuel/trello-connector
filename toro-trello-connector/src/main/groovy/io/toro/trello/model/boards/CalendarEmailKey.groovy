package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Generate Board calendar key model
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class CalendarEmailKey {
	String id
	BoardPrefs myPrefs
}
