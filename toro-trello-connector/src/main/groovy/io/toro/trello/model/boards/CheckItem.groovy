package io.toro.trello.model.boards

import groovy.transform.ToString

@ToString( includeNames = true )
class CheckItem {

	String state
	String id
	String name
	Map nameData
	int pos
}
