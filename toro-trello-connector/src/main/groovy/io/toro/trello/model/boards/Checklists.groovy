package io.toro.trello.model.boards

import groovy.transform.ToString

@ToString( includeNames = true, ignoreNulls = true )
class Checklists {
	String id
	String name
	String idBoard
	String idCard
	int pos
	CheckItem[] checkItems
}
