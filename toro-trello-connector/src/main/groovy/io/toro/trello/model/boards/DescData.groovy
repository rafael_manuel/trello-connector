package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Boards desc data entity model
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class DescData {
	Map<String,?> emoji
}
