package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Action display entity model
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class DisplayEntities {
	ActionEntities contextOn
	ActionEntities card
	ActionEntities comment
	BoardMember memberCreator
}
