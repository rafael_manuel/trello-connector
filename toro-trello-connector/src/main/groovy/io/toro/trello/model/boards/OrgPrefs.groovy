package io.toro.trello.model.boards

import groovy.transform.ToString

/**
 * Organization prefs
 * @author charlesturla
 *
 */

@ToString( includeNames = true )
class OrgPrefs {
	String permissionLevel
	List<?> orgInviteRestrict
	Boolean externalMembersDisabled
	Object associatedDomain
	Integer googleAppsVersion
	BoardRestrictions boardVisibilityRestrict
}
