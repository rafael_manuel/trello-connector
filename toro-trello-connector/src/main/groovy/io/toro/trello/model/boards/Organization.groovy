package io.toro.trello.model.boards

import groovy.transform.ToString
import io.toro.trello.model.fields.Membership

/**
 * Organization entity
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class Organization {
	String id
	String name
	String displayName
	String desc
	DescData descData
	List<String> idBoards
	Boolean invited
	List<?> invitations
	List<Membership> memberships
	OrgPrefs prefs
	List<Integer> powerUps
	List<Integer> products
	Integer billableMemberCount
	Integer activeBillableMemberCount
	String url
	String website
	String logoHash
	List<String> premiumFeatures
}
