package io.toro.trello.model.boards

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Fields for updating a board
 * @author charlesturla
 *
 */

@ToString( includeNames = true, includeSuper = true )
class UpdateBoard extends BasicBoard {
	Boolean closed
	Boolean subscribed
	@XmlElement( name = 'prefs/calendarFeedEnabled' )
	String prefsCalendarFeedEnabled
	@XmlElement( name = 'green' )
	String labelNameGreen
	@XmlElement( name = 'labelNames/yellow' )
	String labelNameYellow
	@XmlElement( name = 'labelNames/orang' )
	String labelNameOrange
	@XmlElement( name = 'labelNames/red' )
	String labelNameRed
	@XmlElement( name = 'labelNames/purple' )
	String labelNamePurple
	@XmlElement( name = 'labelNames/blue' )
	String labelNameBlue
}
