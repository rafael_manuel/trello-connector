package io.toro.trello.model.cards

import groovy.transform.ToString

/**
 * Add new card
 * @author charlesturla
 *
 */

@ToString( includeNames = true, includeSuper = true )
class AddCard extends BasicCard {
	String urlSource
	String fileSource
	String idCardSource
	String keepFromSource
}
