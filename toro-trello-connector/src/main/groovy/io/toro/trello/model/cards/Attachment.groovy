package io.toro.trello.model.cards

import groovy.transform.ToString

/**
 * Card attachment
 * @author charlesturla
 *
 */
@ToString( includeNames = true, ignoreNulls = true )
class Attachment {
	String id
	String bytes
	Date date
	String edgeColor
	String url
	Boolean isUpload
	String mimeType
	String name
	String idMember
	List<?> previews
}
