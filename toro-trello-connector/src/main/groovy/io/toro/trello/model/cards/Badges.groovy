package io.toro.trello.model.cards

import groovy.transform.ToString

/**
 * Card badges entity model
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class Badges {
	Integer votes
	Boolean viewingMemberVoted
	Boolean subscribed
	String fogbugz
	Integer checkItems
	Integer checkItemsChecked
	Integer comments
	Integer attachments
	Boolean description
	Date due
}
