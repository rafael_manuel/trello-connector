package io.toro.trello.model.cards

import groovy.transform.ToString

/**
 * Trello card basic entity for adding and updating
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class BasicCard {
	String name
	String desc
	String pos
	List<String> labels
	List<String> idMembers
	String idList
	String due
}
