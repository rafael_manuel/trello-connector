package io.toro.trello.model.cards

import groovy.transform.ToString
import io.toro.trello.model.boards.Boards
import io.toro.trello.model.fields.Membership

/**
 * Card board entity model
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class CardBoard extends Boards {
	Boolean invited
	Boolean starred
	List<?> invitations
	Boolean subscribed
	List<Membership> memberships
	List<String> powerUps
	Date dateLastActivity
	Date dateLastView
	List<?> idTags
}
