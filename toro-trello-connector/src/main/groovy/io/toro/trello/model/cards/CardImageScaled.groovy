package io.toro.trello.model.cards

import groovy.transform.ToString
import io.toro.trello.model.fields.BackgroundImageScaled
import javax.xml.bind.annotation.XmlElement

/**
 * Card image scaled entity model
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class CardImageScaled extends BackgroundImageScaled {
	@XmlElement( name = '_id' )
	String id
	boolean scaled
}
