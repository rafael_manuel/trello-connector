package io.toro.trello.model.cards

import groovy.transform.ToString

/**
 * Card check item state model
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class CheckItemState {
	String idCheckItem
	String state
}
