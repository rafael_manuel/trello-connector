package io.toro.trello.model.cards

import groovy.transform.ToString

/**
 * Card Label
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class Label {
	String id
	String idBoard
	String color
	String name
	Integer uses
}
