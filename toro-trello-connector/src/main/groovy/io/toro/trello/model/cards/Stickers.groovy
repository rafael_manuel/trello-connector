package io.toro.trello.model.cards

import groovy.transform.ToString

/**
 * Card stickers entity model
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class Stickers {
	String id
	Integer top
	Double left
	Integer zIndex
	Integer rotate
	String image
	String imageUrl
	List<CardImageScaled> imageScaled
}
