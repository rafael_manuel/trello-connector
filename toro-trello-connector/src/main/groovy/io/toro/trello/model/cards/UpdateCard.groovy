package io.toro.trello.model.cards

import groovy.transform.ToString

/**
 * Fields required
 * @author charlesturla
 *
 */
@ToString( includeNames = true, includeSuper = true )
class UpdateCard extends BasicCard {
	String idAttachmentCover
	Boolean closed
	Boolean subscribed
}
