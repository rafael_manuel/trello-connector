package io.toro.trello.model.checklists

import groovy.transform.ToString

/**
 * Add Check item model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class AddCheckItem extends CheckItem {

	boolean checked

}
