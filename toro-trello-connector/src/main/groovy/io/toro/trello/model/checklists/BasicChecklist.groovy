package io.toro.trello.model.checklists

import groovy.transform.ToString

/**
 *
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class BasicChecklist {

	String idCard
	String name
	int pos
	String idChecklistSource

}
