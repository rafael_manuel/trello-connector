package io.toro.trello.model.checklists

import groovy.transform.ToString

/**
 * Card check item model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class CheckItem {

	String state
	String id
	String name
	Map nameData
	int pos

}
