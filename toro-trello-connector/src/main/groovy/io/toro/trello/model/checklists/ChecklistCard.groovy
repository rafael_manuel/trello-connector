package io.toro.trello.model.checklists

import groovy.transform.ToString
import io.toro.trello.model.cards.Card
import io.toro.trello.model.cards.CheckItemState
import io.toro.trello.model.cards.Badges

/**
 * Trello checklist entity model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true, ignoreNulls = true )
class ChecklistCard extends Card {

//	String id
//	CheckItemState[] checkItemStates
//	boolean closed
//	String dateLastActivity
//	String desc
//	String descData
//	String idBoard
//	String idList
//	String[] idMembersVoted
//	int idShort
//	String idAttachmentCover
//	boolean manualCoverAttachment
//	String[] idLabels
//	String name
//	int pos
//	String shortLink
//	Badges badges
//	String due
//	String email
//	String[] idCheckLists
//	String[] idMembers
//	String[] labels
//	String[] shortUrl
//	boolean subscribed
//	String url

}
