package io.toro.trello.model.checklists

import groovy.transform.ToString

/**
 * Trello checklist entity model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true, ignoreNulls = true )
class Checklists {
	String id
	String name
	String idBoard
	String idCard
	int pos
	CheckItem[] checkItems
}
