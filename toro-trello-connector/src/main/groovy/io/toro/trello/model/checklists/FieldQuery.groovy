package io.toro.trello.model.checklists

import groovy.transform.ToString

/**
 * Field get query
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class FieldQuery {
	String fields
	String filter
	String name
	String pos
	String value
}
