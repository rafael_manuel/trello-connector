package io.toro.trello.model.boards

import groovy.transform.ToString

@ToString( includeNames = true )
class LabelNames {
	String id
	DisplayEntities entities
}
