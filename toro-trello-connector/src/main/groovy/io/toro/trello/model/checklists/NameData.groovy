package io.toro.trello.model.checklists

import groovy.transform.ToString

/**
 * Name Data
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class NameData {
	Map emoji
}
