package io.toro.trello.model.fields

import groovy.transform.ToString

/**
 * Prefs background image scale
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class BackgroundImageScaled {
	Integer width
	Integer height
	String url
}
