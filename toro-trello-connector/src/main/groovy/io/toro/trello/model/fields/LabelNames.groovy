package io.toro.trello.model.fields

/**
 * Label names model
 * @author charlesturla
 *
 */
class LabelNames {
	String black
	String blue
	String green
	String lime
	String orange
	String pink
	String purple
	String red
	String sky
	String yellow
}
