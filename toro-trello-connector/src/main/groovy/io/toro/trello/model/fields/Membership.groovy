package io.toro.trello.model.fields

import groovy.transform.ToString
import io.toro.trello.model.boards.BoardMember

/**
 * Board membership entity model
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class Membership {
	String id
	String idMember
	String memberType
	Boolean unconfirmed
	Boolean deactivated
	BoardMember member
}
