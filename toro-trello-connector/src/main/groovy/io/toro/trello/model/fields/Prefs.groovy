package io.toro.trello.model.fields

import groovy.transform.ToString

/**
 * Board preferences entity model
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class Prefs {

	String permissionLevel
	String voting
	String comments
	String invitations
	Boolean selfJoin
	Boolean cardCovers
	String cardAging
	Boolean calendarFeedEnabled
	String[] orgInviteRestrict
	Boolean externalMembersDisabled
	String associatedDomain
	Integer googleAppsVersion
	Map boardVisibilityRestrict
	String background
	String backgroundColor
	String backgroundImage
	List<BackgroundImageScaled> backgroundImageScaled
	Boolean backgroundTile
	String backgroundBrightness
	Boolean canBePublic
	Boolean canBeOrg
	Boolean canBePrivate
	Boolean canInvite
}
