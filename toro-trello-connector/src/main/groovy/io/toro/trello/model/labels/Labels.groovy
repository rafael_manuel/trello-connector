package io.toro.trello.model.labels

import groovy.transform.ToString

/**
 * Trello label entity model
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class Labels {
	String id
	String idBoard
	String name
	String color
	Integer uses
}
