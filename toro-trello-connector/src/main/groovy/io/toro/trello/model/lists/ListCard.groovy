package io.toro.trello.model.lists

import groovy.transform.ToString

/**
 * LabelCard list entity model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ListCard {

	String id
	String name

}
