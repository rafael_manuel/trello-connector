package io.toro.trello.model.lists

import groovy.transform.ToString
import io.toro.trello.model.cards.Card

/**
 * Trello list entity model
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class Lists {

	String id
	String name
	Boolean closed
	String idBoard
	Integer pos
	Card[] cards

}
