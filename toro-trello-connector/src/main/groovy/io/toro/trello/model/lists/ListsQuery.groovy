package io.toro.trello.model.lists

import groovy.transform.ToString

/**
 *  get list query
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true )
class ListsQuery {

	String name
	Boolean closed
	String idBoard
	Integer pos
	Boolean subscribed

}
