package io.toro.trello.model.members

import groovy.transform.ToString
import io.toro.trello.model.cards.Card

/**
 * Trello member entity model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class Member {

	String id
	String avatarHash
	String bio
	String bioData
	Boolean confirmed
	String fullName
	String[] idPremOrgsAdmin
	String initials
	String memberType
	String[] products
	String status
	String url
	String username
	String avatarSource
	String email
	String gravatarHash
	String[] idBoards
	String[] idOrganizations
	String loginTypes
	String oneTimeMessagesDismissed
	String prefs
	String[] trophies
	String uploadedAvatarHash
	String[] premiumFeatures
	String idBoardsPinned

}
