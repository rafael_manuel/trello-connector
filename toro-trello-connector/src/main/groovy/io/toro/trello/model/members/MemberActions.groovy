package io.toro.trello.model.members

import groovy.transform.ToString

/**
 * Trello MemberActions entity model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberActions {

	String id
	MemberData data
	String date
	String idMemberCreator
	String type
	MemberCreator memberCreator

}
