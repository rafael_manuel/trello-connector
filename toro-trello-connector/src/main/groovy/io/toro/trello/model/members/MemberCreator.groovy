package io.toro.trello.model.members

import groovy.transform.ToString

/**
 * Trello MemberCreator entity model
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class MemberCreator {

	String id
	String avatarHash
	String fullName
	String initials
	String username

}
