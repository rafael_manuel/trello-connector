package io.toro.trello.model.members

import groovy.transform.ToString

/**
 * Trello MemberData entity model
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class MemberData {

	Map board
	Map boardSource
	Map boardTarget
	Map old
	Map organization
	Map checklist
	Map card
	Map list
	Map listAfter
	Map listBefore
	Map label
	Map member
	String text

}
