package io.toro.trello.model.members

import groovy.transform.ToString

/**
 * Trello member entity model
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class MemberNotification {

	String id
	Boolean unread
	String type
	Date date
	Map data
	String idMemberCreator
	MemberCreator memberCreator

}
