package io.toro.trello.model.organizations

import groovy.transform.ToString
import io.toro.trello.model.members.MemberCreator
import io.toro.trello.model.members.MemberData

/**
 * Trello organizations actions entity model
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class OrganizationActions {

	String id
	String idMemberCreator
	MemberData data
	String type
	Date date
	MemberCreator memberCreator

}
