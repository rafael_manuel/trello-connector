package io.toro.trello.model.organizations

import groovy.transform.ToString
import io.toro.trello.model.boards.Boards
import io.toro.trello.model.fields.Membership

/**
 *
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class OrganizationBoards extends Boards {

	String invitations
	String[] powerUps
	Date dateLastActivity
	String[] idTags
	Boolean invited
	Boolean starred
	Membership[] memberships
	Boolean subscribed
	Date dateLastView

}
