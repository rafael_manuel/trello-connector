package io.toro.trello.model.organizations

import groovy.transform.ToString
import io.toro.trello.model.members.Member
import io.toro.trello.model.boards.DescData

/**
 * Trello organizations entity model
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class Organizations {

	String id
	String name
	String displayName
	String desc
	Member[] members
	DescData descData
	String url
	String website
	String logoHash
	String[] products
	String[] powerUps

}
