package io.toro.trello.model.query

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Board get actions query
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class BoardActionQuery {
	Boolean entities
	Boolean display
	String filter
	String fields
	String limit
	String format
	Date since
	Date before
	String page
	List<String> idModels
	Boolean member
	@XmlElement( name = 'member_fields' )
	String memberFields
	Boolean memberCreator
	@XmlElement( name = 'memberCreator_fields' )
	String memberCreatorFields
}
