package io.toro.trello.model.query

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Board cards get query
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class BoardCardQuery {
	@XmlElement( defaultValue = 'false' )
	Boolean stickers
	Integer limit
	Date since
	Date before
	@XmlElement( defaultValue = 'visible' )
	String filter
	String actions
	@XmlElement( defaultValue = 'false' )
	Boolean attachments
	@XmlElement( name = 'attachment_fields', defaultValue = 'all' )
	String attachmentFields
	@XmlElement( defaultValue = 'false' )
	Boolean members
	@XmlElement( name = 'member_fields' )
	String memberFields
	Boolean checkItemStates
	String checklists
	String fields
}
