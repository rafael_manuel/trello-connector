package io.toro.trello.model.query

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Board get checklists query
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class BoardChecklistQuery {
	String cards
	@XmlElement( name = 'card_fields' )
	String cardFields
	String checkItems
	@XmlElement( name = 'checkItem_fields' )
	String checkItemFields
	String fields
	String filter
}
