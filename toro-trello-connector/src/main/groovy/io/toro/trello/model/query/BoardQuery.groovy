package io.toro.trello.model.query

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Board get query
 * @author charlesturla
 *
 */
@ToString( includeNames = true, includeSuper = true )
class BoardQuery extends GetQuery {
	@XmlElement( name = 'actions_format' )
	Boolean actionsFormat
	@XmlElement( name = 'actions_since' )
	String actionSince
	String cards
	@XmlElement( name = 'card_fields' )
	String cardFields
	@XmlElement( name = 'card_checklists' )
	String cardChecklists
	@XmlElement( name = 'card_stickers' )
	Boolean cardStickers
	String boardStars
	String labels
	@XmlElement( name = 'label_fields' )
	String labelFields
	@XmlElement( name = 'labels_limit' )
	Integer labelsLimit
	String memberships
	@XmlElement( name = 'memberships_member' )
	Boolean membershipsMember
	@XmlElement( name = 'memberships_member_fields' )
	String membershipsMemberFields
	String membersInvited
	@XmlElement( name = 'membersInvited_fields' )
	String membersInvitedFields
	Boolean organization
	@XmlElement( name = 'organization_fields' )
	String organizationFields
	@XmlElement( name = 'organization_memberships' )
	String organizationMemberships
	Boolean myPrefs
}
