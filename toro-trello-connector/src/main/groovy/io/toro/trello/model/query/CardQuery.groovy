package io.toro.trello.model.query

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Card query model
 * @author charlesturla
 *
 */
@ToString( includeNames = true, includeSuper = true )
class CardQuery extends GetQuery {
	Boolean membersVoted
	String memverVotedFields
	Boolean checkItemStates
	@XmlElement( name = 'checkItemState_fields' )
	String checkItemStateFields
	Boolean board
	String boardFields
	@XmlElement( name = 'list' )
	Boolean boardList
	String listFields
	Boolean stickers
	String stickerFields
}
