package io.toro.trello.model.query

import groovy.transform.ToString
import io.toro.trello.model.checklists.FieldQuery

/**
 * Checklist queries
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true )
class ChecklistQuery extends FieldQuery {

	String idCard
	String idChecklistSource

}
