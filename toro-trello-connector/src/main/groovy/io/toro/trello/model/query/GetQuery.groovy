package io.toro.trello.model.query

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlElements

/**
 * basic get query
 * @author charlesturla
 *
 */
@ToString( includeNames = true, includeSuper = true )
class GetQuery extends ActionQuery {
	Boolean members
	@XmlElement( name = 'member_fields' )
	String memberFields
	String list
	@XmlElement( name = 'list_fields' )
	String listFields
	String fields
	@XmlElements( [
		@XmlElement( name = 'card_attachments' ),
		@XmlElement( name = 'attachments' )
	] )
	Boolean cardAttachments
	@XmlElements( [
		@XmlElement( name = 'card_attachment_fields' ),
		@XmlElement( name = 'attachement_fields' )
	] )
	String cardAttachmentsFields
	String checklists
	@XmlElement( name = 'checklist_fields' )
	String checklistFields
}
