package io.toro.trello.model.query

import groovy.transform.ToString

/**
 * Label get query
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class LabelQuery {

	String id
	String idBoard
	String name
	String color
	int uses
}
