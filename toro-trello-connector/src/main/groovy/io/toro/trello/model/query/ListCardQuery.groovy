package io.toro.trello.model.query

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * list card get query
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true )
class ListCardQuery {

	String actions
	String attachments
	@XmlElement( name = 'attachment_fields' )
	String attachmentFields
	String stickers
	Boolean members
	@XmlElement( name = 'member_fields' )
	String memberFields
	String checkItemStates
	String checklists
	String limit
	String since
	String before
	String filter
	String fields

}
