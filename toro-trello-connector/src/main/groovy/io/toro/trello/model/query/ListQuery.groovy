package io.toro.trello.model.query

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Board get list query
 * @author charlesturla
 *
 */
@ToString( includeNames = true )
class ListQuery {
	String cards
	@XmlElement( name = 'card_fields' )
	String cardFields
	String filter
	String fields
}
