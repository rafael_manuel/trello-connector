package io.toro.trello.model.query

import groovy.transform.ToString

import javax.xml.bind.annotation.XmlElement

/**
 * Basic organization queries
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true )
class OrganizationQuery extends ActionQuery {

	String memberships
	@XmlElement( name = 'memberships_member' )
	Boolean membershipsMember
	@XmlElement( name = 'memberships_member_fields' )
	String membershipsMemberFields
	String members
	@XmlElement( name = 'member_fields' )
	String memberFields
	@XmlElement( name = 'member_activity' )
	Boolean memberActivity
	String membersInvited
	@XmlElement( name = 'membersInvited_fields' )
	String merbersInvitedFields
	String boards
	@XmlElement( name = 'board_fields' )
	String boardFields
	@XmlElement( name = 'board_actions' )
	String boardActions
	@XmlElement( name = 'board_actions_entities' )
	Boolean boardActionsEntities
	@XmlElement( name = 'board_actions_display' )
	Boolean boardActionsDisplay
	@XmlElement( name = 'board_actions_format' )
	String boardActionsFormat
	@XmlElement( name = 'board_actions_since' )
	Date boardctionsSince
	@XmlElement( name = 'board_actions_limit' )
	Integer boardActionsLimit
	@XmlElement( name = 'board_actions_fields' )
	String boardActionFields
	@XmlElement( name = 'board_lists' )
	String boardLists
	@XmlElement( name = 'paid_account' )
	Boolean paidAccount
	String fields

}
