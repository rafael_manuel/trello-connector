package io.toro.trello.model.query

import groovy.transform.ToString

/**
 * basic search member query
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class SearchMemberQuery {

	String query
	Integer limit
	String idBoard
	String idOrganization
	Boolean onlyOrgMembers

}
