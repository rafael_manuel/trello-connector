package io.toro.trello.model.search

import groovy.transform.ToString
import io.toro.trello.model.boards.Boards
import io.toro.trello.model.cards.Card

/**
 * Trello search entity model
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class Search {

	SearchOption options
	Boards[] boards
	Card[] cards
	Map[] organizations
	Map[] members

}
