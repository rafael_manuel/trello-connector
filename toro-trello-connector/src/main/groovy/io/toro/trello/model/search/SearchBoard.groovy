package io.toro.trello.model.search

import groovy.transform.ToString

/**
 * Trello search board entity model
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class SearchBoard {

	String id
	String name
	String idOrganization

}
