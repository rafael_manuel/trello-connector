package io.toro.trello.model.search

import groovy.transform.ToString

/**
 * Trello search member entity model
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class SearchMember {

	String id
	String username
	String avatarHash
	String initials
	String fullName
	String email
	String[] idBoards
	String[] idOrganizations
	Boolean confirmed
	String similarity
	Boolean active

}
