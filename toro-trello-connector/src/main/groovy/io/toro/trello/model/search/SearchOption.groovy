package io.toro.trello.model.search

import groovy.transform.ToString

/**
 * Trello search option entity model
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class SearchOption {

	Map[] terms
	String[] modifiers
	String[] modelTypes
	Boolean partial

}
