package io.toro.trello.model.tokens

import groovy.transform.ToString

/**
 *
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true )
class Permissions {
	String idModel
	String modelType
	Boolean read
	Boolean write
}
