package io.toro.trello.model.tokens

import groovy.transform.ToString

/**
 *
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true )
class Token {
	String id
	String identifier
	String idMember
	Date dateCreated
	Date dateExpires
	Permissions[] permissions
}
