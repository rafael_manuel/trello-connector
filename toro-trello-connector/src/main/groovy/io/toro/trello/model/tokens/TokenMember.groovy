package io.toro.trello.model.tokens

import groovy.transform.ToString

/**
 *
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true )
class TokenMember {
	String id
	String avatarHash
	String bio
	String bioData
	Boolean confirmed
	String fullName
	String[] idPremOrgsAdmin
	String initials
	String memberType
	String[] products
	String status
	String url
	String username
	String avatarSource
	String email
	String gravatarHash
	String[] idBoards
	String[] idOrganizations
	String loginTypes
	String oneTimeMessagesDismissed
	String prefs
	String[] trophies

}
