package io.toro.trello.model.tokens

import groovy.transform.ToString

/**
 * @author rafaelmanuel
 */
@ToString( includeNames = true )
class TokenWebhook {
	String id
	String description
	String idModel
	String callbackURL
	Boolean active
}
