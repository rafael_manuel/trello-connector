package io.toro.trello.model.types

import groovy.transform.ToString

/**
 * Trello type entity model
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class Types {

	String type
	String id

}
