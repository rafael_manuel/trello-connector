package io.toro.trello.model.webhooks

import groovy.transform.ToString

/**
 * 
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true )
class Webhook {

	String id
	String description
	String idModel
	String callbackURL
	Boolean active
}

