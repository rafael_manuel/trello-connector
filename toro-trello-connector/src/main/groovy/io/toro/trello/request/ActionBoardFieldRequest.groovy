package io.toro.trello.request

/**
 * Trello action board  field request
 * @author rafaelmanuel
 *
 */

class ActionBoardFieldRequest {

	String field

}
