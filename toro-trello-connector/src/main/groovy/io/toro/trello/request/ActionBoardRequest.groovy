package io.toro.trello.request

/**
 * Trello action board request
 * @author rafaelmanuel
 *
 */

class ActionBoardRequest {

	String fields

}
