package io.toro.trello.request

/**
 * Trello action card field request
 * @author rafaelmanuel
 *
 */

class ActionCardFieldRequest {

	String field

}
