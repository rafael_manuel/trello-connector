package io.toro.trello.request

/**
 * Trello action field request
 * @author rafaelmanuel
 *
 */

class ActionFieldRequest {

	String field

}
