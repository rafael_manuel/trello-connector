package io.toro.trello.request

/**
 * Trello action list request
 * @author rafaelmanuel
 *
 */

class ActionListRequest {

	String fields

}
