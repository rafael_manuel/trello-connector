package io.toro.trello.request

/**
 * Trello action member creator field request
 * @author rafaelmanuel
 *
 */

class ActionMemberCreatorFieldRequest {

	String field

}
