package io.toro.trello.request

/**
 * Trello action member creator request
 * @author rafaelmanuel
 *
 */

class ActionMemberCreatorRequest {

	String fields

}
