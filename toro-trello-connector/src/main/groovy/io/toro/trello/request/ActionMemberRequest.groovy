package io.toro.trello.request

/**
 * Trello action member request
 * @author rafaelmanuel
 *
 */

class ActionMemberRequest {

	String fields

}
