package io.toro.trello.request

/**
 * Trello action organization fieldrequest
 * @author rafaelmanuel
 *
 */

class ActionOrganizationFieldRequest {

	String field

}
