package io.toro.trello.request

/**
 * Trello action organization request
 * @author rafaelmanuel
 *
 */

class ActionOrganizationRequest {

	String fields

}
