package io.toro.trello.request

import javax.xml.bind.annotation.XmlElement

/**
 * Trello action request
 * @author rafaelmanuel
 *
 */

class ActionRequest {

	Boolean entities
	Boolean display
	String filter
	String fields
	String limit
	String format
	Date since
	Date before
	String page
	List<String> idModels
	Boolean member
	@XmlElement( name = 'member_fields' )
	String memberFields
	Boolean memberCreator
	@XmlElement( name = 'memberCreator_fields' )
	String memberCreatorFields

}
