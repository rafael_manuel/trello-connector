package io.toro.trello.request

/**
 * Trello action text request
 * @author rafaelmanuel
 *
 */

class ActionTextRequest {

	String text

}
