package io.toro.trello.request

/**
 * Trello action text value request
 * @author rafaelmanuel
 *
 */

class ActionTextValueRequest {

	String value

}
