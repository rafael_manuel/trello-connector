package io.toro.trello.request

/**
 * Trello checklist board field request
 * @author rafaelmanuel
 *
 */

class ChecklistBoardFieldRequest {

	String field

}
