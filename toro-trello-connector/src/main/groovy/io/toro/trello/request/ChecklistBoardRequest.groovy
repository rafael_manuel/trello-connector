package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello checklist board request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ChecklistBoardRequest {

	String fields

}
