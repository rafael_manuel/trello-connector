package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello checklist card request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ChecklistCardFilterRequest {

	String filter

}
