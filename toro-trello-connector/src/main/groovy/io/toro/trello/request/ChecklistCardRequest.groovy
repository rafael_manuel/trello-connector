package io.toro.trello.request

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello checklist card request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ChecklistCardRequest {

	Boolean membersVoted
	String memverVotedFields
	Boolean checkItemStates
	@XmlElement( name = 'checkItemState_fields' )
	String checkItemStateFields
	Boolean board
	String boardFields
	@XmlElement( name = 'list' )
	Boolean boardList
	String listFields
	Boolean stickers
	String stickerFields

}
