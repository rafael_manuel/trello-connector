package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello checklist request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ChecklistCheckItemByIdRequest {

	String fields
	String idCheckItem

}
