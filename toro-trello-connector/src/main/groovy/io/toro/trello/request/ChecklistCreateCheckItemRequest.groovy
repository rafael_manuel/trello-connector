package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello checklist create checkitem request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ChecklistCreateCheckItemRequest {

	String name
	Integer pos
	Boolean checked

}
