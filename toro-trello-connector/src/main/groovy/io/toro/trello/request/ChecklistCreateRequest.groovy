package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello checklist create request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ChecklistCreateRequest {

	String idCard
	String name
	Integer pos
	String idChecklistSource

}
