package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello checklist field request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ChecklistFieldRequest {

	String field

}
