package io.toro.trello.request

import groovy.transform.ToString

import javax.xml.bind.annotation.XmlElement

/**
 * Trello checklist request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ChecklistRequest {

	String cards
	@XmlElement( name = 'card_fields' )
	String cardFields
	String checkItems
	@XmlElement( name = 'checkItem_fields' )
	String checkItemFields
	String fields
	String filter

}
