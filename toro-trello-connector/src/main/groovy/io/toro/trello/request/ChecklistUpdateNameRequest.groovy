package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello checklist update name request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ChecklistUpdateNameRequest {

	String value

}
