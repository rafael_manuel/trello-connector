package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello checklist update pos request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ChecklistUpdatePosRequest {

	Integer value

}
