package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello checklist update request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ChecklistUpdateRequest {

	String name
	Integer pos

}
