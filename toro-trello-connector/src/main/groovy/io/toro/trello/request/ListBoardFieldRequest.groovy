package io.toro.trello.request

/**
 * Trello list board field request
 * @author rafaelmanuel
 *
 */

class ListBoardFieldRequest {

	String field

}
