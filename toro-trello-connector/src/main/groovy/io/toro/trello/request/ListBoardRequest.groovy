package io.toro.trello.request

/**
 * Trello list board request
 * @author rafaelmanuel
 *
 */

class ListBoardRequest {

	String fields

}
