package io.toro.trello.request

/**
 * Trello list card filter request
 * @author rafaelmanuel
 *
 */

class ListCardFilterRequest {

	String filter

}
