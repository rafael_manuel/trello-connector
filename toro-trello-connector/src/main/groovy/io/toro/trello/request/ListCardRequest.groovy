package io.toro.trello.request

import javax.xml.bind.annotation.XmlElement

/**
 * Trello list card request
 * @author rafaelmanuel
 *
 */
class ListCardRequest {

	String actions
	String attachments
	@XmlElement( name = 'attachment_fields' )
	String attachmentFields
	String stickers
	Boolean members
	@XmlElement( name = 'member_fields' )
	String memberFields
	String checkItemStates
	String checklists
	String limit
	String since
	String before
	String filter
	String fields

}
