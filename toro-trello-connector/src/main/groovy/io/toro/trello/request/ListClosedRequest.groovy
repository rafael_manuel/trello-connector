package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello list update closed value request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ListClosedRequest {

	Boolean value

}
