package io.toro.trello.request

/**
 * Trello list create card request
 * @author rafaelmanuel
 *
 */

class ListCreateCardRequest {

	String name
	String desc
	String[] labels
	String idMembers
	Date due

}
