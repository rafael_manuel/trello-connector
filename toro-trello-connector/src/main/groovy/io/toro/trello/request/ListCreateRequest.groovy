package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello list create request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ListCreateRequest extends ListUpdateRequest {

}
