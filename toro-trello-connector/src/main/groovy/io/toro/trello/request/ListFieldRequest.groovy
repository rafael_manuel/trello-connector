package io.toro.trello.request

/**
 * Trello list field request
 * @author rafaelmanuel
 *
 */

class ListFieldRequest {

	String field

}
