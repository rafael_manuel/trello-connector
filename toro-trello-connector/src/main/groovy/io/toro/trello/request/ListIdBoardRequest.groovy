package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello list update idBoard value request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ListIdBoardRequest {

	String value
	String pos

}
