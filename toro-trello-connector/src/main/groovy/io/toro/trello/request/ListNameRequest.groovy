package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello list update name value request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ListNameRequest {

	String value

}
