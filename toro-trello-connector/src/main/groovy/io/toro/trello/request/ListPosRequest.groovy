package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello list update pos value request
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true )
class ListPosRequest {

	String value

}
