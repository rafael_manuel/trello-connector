package io.toro.trello.request

import javax.xml.bind.annotation.XmlElement

/**
 * Trello list request
 * @author rafaelmanuel
 *
 */

class ListRequest {

	String cards
	@XmlElement( name = 'card_fields' )
	String cardFields
	String checkItems
	@XmlElement( name = 'checkItem_fields' )
	String checkItemFields
	String fields
	String filter

}
