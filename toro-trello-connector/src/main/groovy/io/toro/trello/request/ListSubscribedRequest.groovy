package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello list update subscribed value request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ListSubscribedRequest {

	Boolean value

}
