package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello list update request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true )
class ListUpdateRequest {

	String name
	Boolean closed
	String idBoard
	Integer pos
	Boolean subscribed

}
