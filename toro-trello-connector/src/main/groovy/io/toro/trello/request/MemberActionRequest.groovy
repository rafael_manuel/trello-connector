package io.toro.trello.request

import javax.xml.bind.annotation.XmlElement

/**
 * Trello member request
 * @author rafaelmanuel
 *
 */

class MemberActionRequest {

	Boolean entities
	Boolean display
	String filter
	String fields
	Integer limit
	String format
	Date since
	Date before
	Integer page
	String idModels
	Boolean member
	@XmlElement( name = 'member_fields' )
	String memberFields
	Boolean memberCreator
	@XmlElement( name = 'memberCreator_fields' )
	String memberCreatorFields

}
