package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello member board background by id request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberBoardBackgroundByIdRequest {

	String idBoardBackground
	String fields

}
