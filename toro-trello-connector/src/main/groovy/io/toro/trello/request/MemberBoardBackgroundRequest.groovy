package io.toro.trello.request

/**
 * Trello member board background request
 * @author rafaelmanuel
 *
 */

class MemberBoardBackgroundRequest {

	String filter

}
