package io.toro.trello.request

/**
 * Trello member board filter request
 * @author rafaelmanuel
 *
 */

class MemberBoardFilterRequest {

	String filter

}
