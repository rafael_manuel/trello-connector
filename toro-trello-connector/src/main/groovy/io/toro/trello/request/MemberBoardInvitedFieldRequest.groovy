package io.toro.trello.request

/**
 * Trello member board invited field request
 * @author rafaelmanuel
 *
 */

class MemberBoardInvitedFieldRequest {

	String field

}
