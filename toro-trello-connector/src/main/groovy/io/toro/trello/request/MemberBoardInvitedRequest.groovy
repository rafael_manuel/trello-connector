package io.toro.trello.request

/**
 * Trello member board invited request
 * @author rafaelmanuel
 *
 */

class MemberBoardInvitedRequest {

	String fields

}
