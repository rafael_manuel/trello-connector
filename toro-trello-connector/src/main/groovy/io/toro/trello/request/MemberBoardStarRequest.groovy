package io.toro.trello.request

/**
 * Trello member board star request
 * @author rafaelmanuel
 *
 */

class MemberBoardStarRequest {

	String idBoardStar

}
