package io.toro.trello.request

/**
 * Trello member field request
 * @author rafaelmanuel
 *
 */

class MemberFieldRequest {

	String field

}
