package io.toro.trello.request

/**
 * Trello member organization filter request
 * @author rafaelmanuel
 *
 */
class MemberOrganizationFilterRequest {

	String filter

}
