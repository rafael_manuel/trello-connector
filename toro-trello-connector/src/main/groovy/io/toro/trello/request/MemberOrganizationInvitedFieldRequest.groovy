package io.toro.trello.request

/**
 * Trello member organizationInvited field request
 * @author rafaelmanuel
 *
 */

class MemberOrganizationInvitedFieldRequest {

	String field

}
