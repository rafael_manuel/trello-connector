package io.toro.trello.request

/**
 * Trello member organizationInvited request
 * @author rafaelmanuel
 *
 */

class MemberOrganizationInvitedRequest {

	String filter

}
