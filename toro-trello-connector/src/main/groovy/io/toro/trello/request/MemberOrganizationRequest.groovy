package io.toro.trello.request

import javax.xml.bind.annotation.XmlElement

/**
 * Trello member organization request
 * @author rafaelmanuel
 *
 */

class MemberOrganizationRequest {

	String filter
	String fields
	@XmlElement(name='paid_account')
	Boolean paidAccount

}
