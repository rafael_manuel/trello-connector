package io.toro.trello.request

import javax.xml.bind.annotation.XmlElement

/**
 * Trello member request
 * @author rafaelmanuel
 *
 */

class MemberRequest {

	String actions
	@XmlElement( name = 'actions_entities' )
	Boolean actionsEntities
	@XmlElement( name = 'actions_display' )
	Boolean actionsDisplay
	@XmlElement( name = 'actions_limit' )
	Integer actionsLimit
	@XmlElement( name = 'action_fields' )
	String actionFields
	@XmlElement( name = 'action_member' )
	Boolean actionMember
	@XmlElement( name = 'action_member_fields' )
	String actionMemberFields
	@XmlElement( name = 'action_memberCreator' )
	Boolean actionMemberCreator
	@XmlElement( name = 'action_memberCreator_fields' )
	String actionMemberCreatorFields

}
