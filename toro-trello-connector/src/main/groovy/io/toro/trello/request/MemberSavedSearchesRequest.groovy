package io.toro.trello.request

/**
 * Trello member savedSearch request
 * @author rafaelmanuel
 *
 */

class MemberSavedSearchesRequest {

	String idSavedSearch

}
