package io.toro.trello.request

/**
 * Trello member token request
 * @author rafaelmanuel
 *
 */

class MemberTokenRequest {

	String filter
	Boolean webhooks

}
