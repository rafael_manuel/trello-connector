package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello notification field request
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class NotificationFieldRequest {

	String field

}
