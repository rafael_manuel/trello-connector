package io.toro.trello.request

/**
 * Trello notification field unread request
 * @author rafaelmanuel
 *
 */

class NotificationFieldUnreadRequest {

	Boolean value

}
