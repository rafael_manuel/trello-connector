package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello notification member creator field request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class NotificationMemberCreatorFieldRequest {

	String field

}
