package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello notification member creator request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class NotificationMemberCreatorRequest {

	String fields

}
