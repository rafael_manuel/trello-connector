package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello notification organization field request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class NotificationOrganizationFieldRequest {

	String field

}
