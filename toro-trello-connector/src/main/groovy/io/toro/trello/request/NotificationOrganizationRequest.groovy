package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello notification organization request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class NotificationOrganizationRequest {

	String fields

}
