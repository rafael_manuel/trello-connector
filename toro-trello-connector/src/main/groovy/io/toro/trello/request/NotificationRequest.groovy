package io.toro.trello.request

/**
 * Trello notification request
 * @author rafaelmanuel
 *
 */

class NotificationRequest {

	Boolean unread

}
