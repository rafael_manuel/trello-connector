package io.toro.trello.request

import groovy.transform.ToString
import io.toro.trello.model.query.ActionQuery
import javax.xml.bind.annotation.XmlElement

/**
 *
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class OrganizationBoardRequest extends ActionQuery {

	String filter
	String fields
	String actions
	@XmlElement(name='actions_since')
	Date actionsSince
	String memberships
	Boolean organization
	@XmlElement(name='organization_fields')
	String organizationFields
	String lists

}
