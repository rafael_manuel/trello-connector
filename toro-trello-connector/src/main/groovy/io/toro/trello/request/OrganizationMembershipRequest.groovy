package io.toro.trello.request

import groovy.transform.ToString

/**
 *
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class OrganizationMembershipRequest {

	String id
	String idMember
	String memberType
	Boolean unconfirmed
	Map member

}
