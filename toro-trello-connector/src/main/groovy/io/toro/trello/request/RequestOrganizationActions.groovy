package io.toro.trello.model.request

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * @author rafaelmanuel
 */

@ToString( includeNames = true)
class RequestOrganizationActions {

	Boolean entities
	Boolean display
	String filter
	String fields
	Integer limit
	String format
	Date since
	Date before
	Integer page
	String idModels
	Boolean member
	@XmlElement( name = 'member_fields' )
	String memberFields
	Boolean memberCreator
	@XmlElement( name = 'memberCreator_fields')
	String memberCreatorFields

}
