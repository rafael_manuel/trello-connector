package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello searc member request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class SearchMemberRequest {

	String query
	Integer limit
	String idBoard
	String idOrganization
	Boolean onlyOrgMembers

}
