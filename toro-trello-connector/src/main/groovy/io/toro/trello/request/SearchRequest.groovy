package io.toro.trello.request

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello searc request
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class SearchRequest {

	String query
	String idBoards
	String idOrganizations
	String idCards
	String modelTypes
	@XmlElement(name = 'board_fields')
	String boardFields
	@XmlElement(name = 'boards_limit')
	Integer boardsLimit
	@XmlElement(name = 'card_fields')
	String cardFields
	@XmlElement(name = 'cards_limit')
	Integer cardsLimit
	@XmlElement(name = 'cards_page')
	Integer cardPage
	@XmlElement(name = 'card_board')
	Boolean cardBoard
	@XmlElement(name = 'card_list')
	Boolean cardList
	@XmlElement(name = 'card_members')
	Boolean cardMembers
	@XmlElement(name = 'card_stickers')
	Boolean cardStickers
	@XmlElement(name = 'card_attachments')
	Boolean cardAttachments
	@XmlElement(name = 'organization_fields')
	String organizationFields
	@XmlElement(name = 'organizations_limit')
	Integer organizationLimit
	@XmlElement(name = 'member_fields')
	String memberFields
	@XmlElement(name = 'members_limit')
	Integer membersLimit
	Boolean partial

}
