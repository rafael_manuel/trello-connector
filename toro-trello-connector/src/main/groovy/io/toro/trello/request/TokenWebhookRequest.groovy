package io.toro.trello.request

import groovy.transform.ToString

/**
 * @author rafaelmanuel
 */

@ToString( includeNames = true)
class TokenWebhookRequest {

	String description
	String idModel
	String callbackURL

}
