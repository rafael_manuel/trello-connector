package io.toro.trello.request

import groovy.transform.ToString

/**
 * Trello type request
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class TypeRequest {

	String id

}
