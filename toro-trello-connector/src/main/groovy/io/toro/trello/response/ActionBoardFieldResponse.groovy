package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello action board field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionBoardFieldResponse {

	@XmlElement(name='_value')
	String value

}
