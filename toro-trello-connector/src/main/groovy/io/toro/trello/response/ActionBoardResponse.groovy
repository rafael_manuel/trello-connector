package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.boards.DescData
import io.toro.trello.model.fields.Prefs
import io.toro.trello.model.fields.Membership
import io.toro.trello.model.fields.LabelNames

/**
 * Trello action board response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionBoardResponse {

	String id
	String name
	String desc
	DescData descData
	Boolean closed
	String idOrganization
	Boolean invited
	Boolean pinned
	Boolean starred
	String url
	Prefs prefs
	String[] invitations
	Membership[] memberships
	Boolean subscribed
	String shortUrl
	String shortLink
	LabelNames labelNames
	String[] powerUps
	Date dateLastActivity
	Date dateLastView
	String[] idTags

}
