package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello action response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionCardFieldResponse {

	@XmlElement(name='_value')
	String value

}
