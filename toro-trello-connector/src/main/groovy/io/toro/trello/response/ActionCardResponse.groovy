package io.toro.trello.response

import groovy.transform.ToString

import io.toro.trello.model.boards.BoardAction
import io.toro.trello.model.boards.BoardList
import io.toro.trello.model.boards.BoardMember
import io.toro.trello.model.boards.Boards
import io.toro.trello.model.boards.DescData
import io.toro.trello.model.cards.Badges
import io.toro.trello.model.cards.Attachment
import io.toro.trello.model.cards.Label
import io.toro.trello.model.cards.Stickers

/**
 * Trello action card response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionCardResponse {

	String id
	Badges badges
	List<?> checkItemStates
	Boolean closed
	Date dateLastActivity
	String desc
	DescData descData
	Date due
	String email
	String idBoard
	List<String> idChecklists
	List<?> idLabels
	String idList
	List<String> idMembers
	List<String> idMembersVoted
	Integer idShort
	String idAttachmentCover
	List<Attachment> attachments
	Boolean manualCoverAttachment
	List<Label> labels
	String name
	Integer pos
	String shortUrl
	String shortLink
	String url
	List<Stickers> stickers
	List<BoardMember> membersVoted
	List<BoardMember> members
	List<BoardList> list
	Boards board
	Boolean subscribed
	List<BoardAction> actions

}
