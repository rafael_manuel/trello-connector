package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.actions.Entities

/**
 * Trello action display response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionDisplayResponse {

	String translationKey
	Entities entities

}
