package io.toro.trello.response

import groovy.transform.ToString

/**
 * Trello action entities response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionEntitiesResponse {

	String id
	String type
	String username
	String text
	String shortLink

}
