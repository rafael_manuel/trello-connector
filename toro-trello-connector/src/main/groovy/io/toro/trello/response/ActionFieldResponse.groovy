package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello action field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionFieldResponse {

	@XmlElement(name='_value')
	String value

}
