package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello action list response
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class ActionListFieldResponse {

	@XmlElement(name='_value')
	String value

}
