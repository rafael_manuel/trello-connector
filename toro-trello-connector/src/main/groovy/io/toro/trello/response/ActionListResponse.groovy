package io.toro.trello.response

import groovy.transform.ToString

/**
 * Trello action list response
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class ActionListResponse {

	String id
	String name
	Boolean closed
	String idBoard
	Integer pos
	Boolean subscribed

}
