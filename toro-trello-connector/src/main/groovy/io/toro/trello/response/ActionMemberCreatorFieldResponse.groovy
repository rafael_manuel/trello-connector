package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello action member creator field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionMemberCreatorFieldResponse {

	@XmlElement(name='_value')
	String value

}
