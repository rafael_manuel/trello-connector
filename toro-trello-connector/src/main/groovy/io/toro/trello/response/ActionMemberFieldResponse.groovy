package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello action member response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionMemberFieldResponse {

	@XmlElement(name='_value')
	String value

}
