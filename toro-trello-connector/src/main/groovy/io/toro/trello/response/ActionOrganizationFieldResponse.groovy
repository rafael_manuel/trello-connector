package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello action organization field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionOrganizationFieldResponse {

	@XmlElement(name='_value')
	String value

}
