package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.boards.DescData
import io.toro.trello.model.members.Member

/**
 * Trello action organization response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionOrganizationResponse {

	String id
	String name
	String displayName
	String desc
	Member[] members
	DescData descData
	String url
	String website
	String logoHash
	String[] products
	String[] powerUps

}
