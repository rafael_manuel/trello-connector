package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.actions.Data
import io.toro.trello.model.members.MemberCreator

/**
 * Trello action response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ActionResponse {

	String id
	String idMemberCreator
	Data data
	String type
	Date date
	MemberCreator memberCreator

}
