package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello checklist board field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ChecklistBoardFieldResponse {

	@XmlElement(name = '_value')
	String value

}
