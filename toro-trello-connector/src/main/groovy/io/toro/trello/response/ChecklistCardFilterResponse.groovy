package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.cards.CheckItemState
import io.toro.trello.model.cards.Badges

/**
 * Trello checklist card field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ChecklistCardFilterResponse {

	String id
	CheckItemState[] checkItemStates
	Boolean closed
	String dateLastActivity
	String desc
	String descData
	String idBoard
	String idList
	String[] idMembersVoted
	Integer idShort
	String idAttachmentCover
	Boolean manualCoverAttachment
	String[] idLabels
	String name
	Integer pos
	String shortLink
	Badges badges
	String due
	String email
	String[] idChecklists
	String[] idMembers
	String[] labels
	String shortUrl
	Boolean subscribed
	String url

}
