package io.toro.trello.response

import groovy.transform.ToString

/**
 * Trello checklist checkitem response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ChecklistCheckItemResponse {

	String state
	String id
	String name
	Map nameData
	int pos

}
