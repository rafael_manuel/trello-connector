package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello checklist field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ChecklistFieldResponse {

	@XmlElement(name = '_value')
	String value

}
