package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.checklists.CheckItem

/**
 * Trello checklist response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ChecklistResponse {

	String id
	String name
	String idBoard
	String idCard
	int pos
	CheckItem[] checkItems

}
