package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.boards.ActionDisplay
import io.toro.trello.model.members.Member
import io.toro.trello.model.members.MemberCreator
import io.toro.trello.model.boards.ActionEntities

/**
 * Trello list action response
 * @author rafaelmanuel
 */
@ToString( includeNames = true)
class ListActionResponse {

	String id
	String idMemberCreator
	Map<String, ?> data
	String type
	Date date
	Member member
	MemberCreator memberCreator
	List<ActionEntities> entities
	ActionDisplay display

}
