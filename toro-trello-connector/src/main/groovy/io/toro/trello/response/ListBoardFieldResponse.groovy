package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello list board  fieldresponse
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ListBoardFieldResponse extends ListBoardResponse {

	@XmlElement(name = '_value')
	String value

}
