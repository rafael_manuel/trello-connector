package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.fields.LabelNames
import io.toro.trello.model.fields.Prefs
import io.toro.trello.model.fields.Membership
import io.toro.trello.model.boards.DescData

/**
 * Trello list board response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ListBoardResponse {

	String id
	String name
	String desc
	DescData descData
	Boolean closed
	String idOrganization
	Boolean invited
	Boolean pinned
	Boolean starred
	String url
	Prefs prefs
	Map[] invitations
	Membership[] memberships
	String shortLink
	Boolean subscribed
	LabelNames labelNames
	Map[] powerUps
	String dateLastActivity
	String dateLastView
	String shortUrl
	Map[] idTags

}
