package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello list field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ListFieldResponse extends ListResponse {

	@XmlElement(name='_value')
	String value

}
