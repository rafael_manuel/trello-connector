package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.cards.Card

/**
 * Trello list response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class ListResponse {

	String id
	String name
	Boolean closed
	String idBoard
	Integer pos
	Card[] cards

}
