package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.members.MemberData
import io.toro.trello.model.members.MemberCreator

/**
 * Trello member action response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberActionResponse {

	String id
	MemberData data
	String date
	String idMemberCreator
	String type
	MemberCreator memberCreator

}
