package io.toro.trello.response

import groovy.transform.ToString

/**
 * Trello member board background response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberBoardBackgroundResponse {

	String id
	String type
	String color
	Boolean tile
	String brightness

}
