package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello member board invited field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberBoardInvitedFieldResponse {

	@XmlElement(name='comming_soon')
	String commingSoon

}
