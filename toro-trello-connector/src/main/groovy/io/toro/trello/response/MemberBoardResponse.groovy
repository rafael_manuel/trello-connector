package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.fields.Membership
import io.toro.trello.model.boards.DescData
import io.toro.trello.model.boards.BoardMember
import io.toro.trello.model.fields.LabelNames
import io.toro.trello.model.fields.Prefs

/**
 * Trello member board response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberBoardResponse {

	String id
	String name
	String desc
	DescData descData
	Boolean closed
	String idOrganization
	Boolean pinned
	String url
	String shortUrl
	String shortLink
	Prefs prefs
	LabelNames labelNames
	List<BoardMember> members
	String invitations
	String[] powerUps
	Date dateLastActivity
	String[] idTags
	Boolean invited
	Boolean starred
	Membership[] memberships
	Boolean subscribed
	Date dateLastView
}
