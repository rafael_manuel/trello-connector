package io.toro.trello.response

import groovy.transform.ToString

/**
 * Trello member board background response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberBoardStarResponse {

	String id
	String idBoard
	Integer pos

}
