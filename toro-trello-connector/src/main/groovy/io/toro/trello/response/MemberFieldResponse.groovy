package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello member field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberFieldResponse extends MemberResponse {

	@XmlElement(name = '_value')
	String value

}
