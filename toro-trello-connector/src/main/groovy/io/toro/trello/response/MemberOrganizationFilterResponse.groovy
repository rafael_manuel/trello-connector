package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.fields.Prefs
import io.toro.trello.model.fields.Membership
import io.toro.trello.model.boards.DescData

/**
 * Trello member organization filter response
 * @author rafaelmanuel
 *
 */
@ToString( includeNames = true)
class MemberOrganizationFilterResponse {

	String id
	Integer billableMemberCount
	Integer activeBillableMemberCount
	String desc
	DescData descData
	String displayName
	String[] idBoards
	String[] invitations
	Boolean invited
	String logoHash
	Membership[] memberships
	String name
	String[] powerUps
	Prefs prefs
	String[] premiumFeatures
	String[] products
	String url
	String website

}
