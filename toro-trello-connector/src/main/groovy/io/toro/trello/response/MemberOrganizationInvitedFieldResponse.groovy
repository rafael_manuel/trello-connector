package io.toro.trello.response

import groovy.transform.ToString

/**
 * Trello member organizationinvited filter response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberOrganizationInvitedFieldResponse {

}
