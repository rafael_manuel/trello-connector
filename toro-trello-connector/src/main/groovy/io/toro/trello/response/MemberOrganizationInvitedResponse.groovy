package io.toro.trello.response

import groovy.transform.ToString

/**
 * Trello member organizationinvited response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberOrganizationInvitedResponse {

}
