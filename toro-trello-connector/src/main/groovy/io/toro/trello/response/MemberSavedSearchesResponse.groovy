package io.toro.trello.response

import groovy.transform.ToString

/**
 * Trello member saveSearches response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class MemberSavedSearchesResponse {

	String id
	String name
	String query
	Integer pos

}
