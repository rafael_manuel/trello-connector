package io.toro.trello.response

import groovy.transform.ToString

/**
 * Trello notification entities response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class NotificationEntitiesResponse {

	String type
	String id
	String username
	String text

}
