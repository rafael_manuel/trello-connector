package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello notification field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class NotificationFieldResponse {

	@XmlElement(name='_value')
	String value

}
