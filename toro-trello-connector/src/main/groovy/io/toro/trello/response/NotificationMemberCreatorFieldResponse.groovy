package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello notification member creator field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class NotificationMemberCreatorFieldResponse {

	@XmlElement(name='_value')
	String value

}
