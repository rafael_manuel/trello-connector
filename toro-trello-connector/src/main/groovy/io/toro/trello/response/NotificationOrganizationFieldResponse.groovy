package io.toro.trello.response

import groovy.transform.ToString
import javax.xml.bind.annotation.XmlElement

/**
 * Trello notification organization field response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class NotificationOrganizationFieldResponse {

	@XmlElement(name = '_value')
	String value

}
