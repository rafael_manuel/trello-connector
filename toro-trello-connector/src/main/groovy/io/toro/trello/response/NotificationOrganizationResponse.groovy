package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.boards.DescData
import io.toro.trello.model.fields.Membership
import io.toro.trello.model.fields.Prefs

/**
 * Trello notification organization response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class NotificationOrganizationResponse {

	String id
	String name
	String displayName
	String desc
	DescData descData
	String[] idBoards
	Boolean invited
	String[] invitations
	Membership[] memberships
	Prefs prefs
	String[] powerUps
	String[] products
	Integer billableMemberCount
	Integer activeBillableMemberCount
	String url
	String website
	String logoHash
	String[] premiumFeatures

}
