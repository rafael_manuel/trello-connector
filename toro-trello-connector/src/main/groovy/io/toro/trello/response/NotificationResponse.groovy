package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.members.MemberCreator

/**
 * Trello notification response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class NotificationResponse {

	String id
	Boolean unread
	String type
	Date date
	Map data
	String idMemberCreator
	MemberCreator memberCreator

}
