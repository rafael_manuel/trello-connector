package io.toro.trello.response

import groovy.transform.ToString

/**
 * Trello search member response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class SearchMemberResponse {

	String id
	String username
	String avatarHash
	String initials
	String fullName
	String email
	String[] idBoards
	String[] idOrganizations
	Boolean confirmed
	String similarity
	Boolean active

}
