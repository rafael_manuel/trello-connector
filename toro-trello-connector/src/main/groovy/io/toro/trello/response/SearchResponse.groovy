package io.toro.trello.response

import groovy.transform.ToString
import io.toro.trello.model.search.SearchOption
import io.toro.trello.model.boards.Boards
import io.toro.trello.model.cards.Card

/**
 * Trello search response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class SearchResponse {

	SearchOption options
	Boards[] boards
	Card[] cards
	Map[] organizations
	Map[] members

}
