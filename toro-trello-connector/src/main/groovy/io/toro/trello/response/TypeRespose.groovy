package io.toro.trello.response

import groovy.transform.ToString

/**
 * Trello type response
 * @author rafaelmanuel
 *
 */

@ToString( includeNames = true)
class TypeResponse {

	String type
	String id

}
