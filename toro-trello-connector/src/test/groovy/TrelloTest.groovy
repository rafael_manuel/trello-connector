import io.toro.trello.connector.TrelloConnector
import io.toro.trello.model.boards.AddBoard
import io.toro.trello.model.boards.BoardAction
import io.toro.trello.model.boards.BoardCard
import io.toro.trello.model.boards.BoardChecklist
import io.toro.trello.model.boards.BoardList
import io.toro.trello.model.boards.BoardMember
import io.toro.trello.model.boards.BoardStars
import io.toro.trello.model.boards.Boards
import io.toro.trello.model.boards.UpdateBoard
import io.toro.trello.model.cards.AddCard
import io.toro.trello.model.cards.Label
import io.toro.trello.model.query.ActionQuery
import io.toro.trello.model.query.BoardActionQuery
import io.toro.trello.model.query.BoardCardQuery
import io.toro.trello.model.query.BoardChecklistQuery
import io.toro.trello.model.query.CardQuery
import io.toro.trello.model.query.ListQuery
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.lang.Unroll
import io.toro.trello.model.checklists.Checklists
import io.toro.trello.model.checklists.FieldQuery
import io.toro.trello.model.checklists.ChecklistCard
import io.toro.trello.model.query.ChecklistQuery
import io.toro.trello.model.checklists.AddChecklist
import io.toro.trello.model.checklists.AddCheckItem

import io.toro.trello.model.labels.Labels
import io.toro.trello.model.labels.LabelBoard
import io.toro.trello.model.query.LabelQuery

import io.toro.trello.model.lists.Lists
import io.toro.trello.model.lists.ListsQuery
import io.toro.trello.model.query.ListCardQuery


import io.toro.trello.model.members.Member
import io.toro.trello.model.members.MemberActions
import io.toro.trello.model.members.MemberData
import io.toro.trello.model.members.MemberCreator
import io.toro.trello.model.query.SearchQuery
import io.toro.trello.model.search.SearchMember
import io.toro.trello.model.query.SearchMemberQuery
import io.toro.trello.model.types.Types
import io.toro.trello.model.organizations.Organizations
import io.toro.trello.model.query.OrganizationQuery
import io.toro.trello.model.organizations.OrganizationActions
import io.toro.trello.model.request.RequestOrganizationActions
import io.toro.trello.request.OrganizationBoardRequest
import io.toro.trello.request.OrganizationMembershipRequest
import io.toro.trello.request.TokenWebhookRequest
import io.toro.trello.model.tokens.Token
import io.toro.trello.model.tokens.TokenMember
import io.toro.trello.model.webhooks.Webhook
import io.toro.trello.model.members.MemberNotification
import io.toro.trello.response.NotificationResponse
import io.toro.trello.request.NotificationRequest
import io.toro.trello.response.NotificationFieldResponse
import io.toro.trello.request.NotificationFieldRequest
import io.toro.trello.response.NotificationEntitiesResponse
import io.toro.trello.response.NotificationMemberCreatorResponse
import io.toro.trello.request.NotificationMemberCreatorRequest
import io.toro.trello.request.NotificationMemberCreatorFieldRequest
import io.toro.trello.response.NotificationMemberCreatorFieldResponse
import io.toro.trello.response.NotificationOrganizationResponse
import io.toro.trello.request.NotificationOrganizationRequest
import io.toro.trello.response.NotificationOrganizationFieldResponse
import io.toro.trello.request.NotificationOrganizationFieldRequest
import io.toro.trello.request.NotificationFieldUnreadRequest
import io.toro.trello.response.SearchResponse
import io.toro.trello.response.SearchMemberResponse
import io.toro.trello.request.SearchRequest
import io.toro.trello.request.SearchMemberRequest
import io.toro.trello.request.TypeRequest
import io.toro.trello.response.TypeResponse
import io.toro.trello.response.MemberOrganizationResponse
import io.toro.trello.request.MemberOrganizationRequest
import io.toro.trello.response.MemberOrganizationFilterResponse
import io.toro.trello.request.MemberOrganizationFilterRequest
import io.toro.trello.request.MemberOrganizationInvitedRequest
import io.toro.trello.request.MemberOrganizationInvitedFieldRequest
import io.toro.trello.request.MemberSavedSearchesRequest
import io.toro.trello.request.*
import io.toro.trello.response.*
import io.toro.trello.request.ActionRequest
import io.toro.trello.response.ActionResponse
import io.toro.trello.response.ActionBoardResponse
import io.toro.trello.request.ActionBoardRequest
import io.toro.trello.response.ActionBoardFieldResponse
import io.toro.trello.request.ActionBoardFieldRequest
import io.toro.trello.response.ActionCardResponse
import io.toro.trello.request.ActionCardRequest
import io.toro.trello.response.ActionDisplayResponse
import io.toro.trello.response.ActionCardFieldResponse
import io.toro.trello.request.ActionCardFieldRequest
import io.toro.trello.response.ActionListResponse
import io.toro.trello.request.ActionListRequest
import io.toro.trello.response.ActionListFieldResponse
import io.toro.trello.request.ActionListFieldRequest
import io.toro.trello.response.ActionMemberResponse
import io.toro.trello.request.ActionMemberRequest
import io.toro.trello.response.ActionMemberFieldResponse
import io.toro.trello.request.ActionMemberFieldRequest
import io.toro.trello.response.ActionMemberCreatorResponse
import io.toro.trello.request.ActionMemberCreatorRequest
import io.toro.trello.response.ActionMemberCreatorFieldResponse
import io.toro.trello.request.ActionMemberCreatorFieldRequest
import io.toro.trello.response.ActionOrganizationResponse
import io.toro.trello.request.ActionOrganizationRequest
import io.toro.trello.response.ActionOrganizationFieldResponse
import io.toro.trello.request.ActionOrganizationFieldRequest
import io.toro.trello.request.ActionTextRequest
import io.toro.trello.request.ActionTextValueRequest
import io.toro.trello.response.ListResponse
import io.toro.trello.request.ListRequest
import io.toro.trello.request.ListUpdateRequest
import io.toro.trello.response.ListFieldResponse
import io.toro.trello.request.ListFieldRequest
import io.toro.trello.response.ListActionResponse
import io.toro.trello.request.ListActionRequest
import io.toro.trello.response.ListBoardResponse
import io.toro.trello.request.ListBoardRequest
import io.toro.trello.response.ListBoardFieldResponse
import io.toro.trello.request.ListBoardFieldRequest
import io.toro.trello.response.ListCardResponse
import io.toro.trello.request.ListCardRequest
import io.toro.trello.response.ListCardFilterResponse
import io.toro.trello.request.ListCardFilterRequest
import io.toro.trello.request.ListClosedRequest
import io.toro.trello.request.ListNameRequest
import io.toro.trello.request.ListPosRequest
import io.toro.trello.request.ListSubscribedRequest
import io.toro.trello.request.ListCreateRequest
import io.toro.trello.response.ListCreateResponse
import io.toro.trello.response.ListArchiveAllCardResponse
import io.toro.trello.request.ListCreateCardRequest
import io.toro.trello.response.ChecklistResponse
import io.toro.trello.request.ChecklistRequest
import io.toro.trello.response.ChecklistFieldResponse
import io.toro.trello.request.ChecklistFieldRequest
import io.toro.trello.response.ChecklistBoardResponse
import io.toro.trello.request.ChecklistBoardRequest
import io.toro.trello.response.ChecklistBoardFieldResponse
import io.toro.trello.request.ChecklistBoardFieldRequest
import io.toro.trello.response.ChecklistCardResponse
import io.toro.trello.request.ChecklistCardRequest
import io.toro.trello.response.ChecklistCardFilterResponse
import io.toro.trello.request.ChecklistCardFilterRequest
import io.toro.trello.response.ChecklistCheckItemResponse
import io.toro.trello.request.ChecklistCheckItemRequest
import io.toro.trello.request.ChecklistCheckItemByIdRequest
import io.toro.trello.request.ChecklistUpdateRequest
import io.toro.trello.request.ChecklistUpdateNameRequest
import io.toro.trello.request.ChecklistUpdatePosRequest
import io.toro.trello.request.ChecklistCreateRequest
import io.toro.trello.request.ChecklistCreateCheckItemRequest
import io.toro.trello.request.MemberRequest
import io.toro.trello.response.MemberResponse
import io.toro.trello.request.MemberFieldRequest
import io.toro.trello.response.MemberFieldResponse
import io.toro.trello.request.MemberActionRequest
import io.toro.trello.response.MemberActionResponse
import io.toro.trello.request.MemberBoardBackgroundByIdRequest
import io.toro.trello.response.MemberBoardStarResponse
import io.toro.trello.request.MemberBoardStarRequest
import io.toro.trello.request.MemberBoardBackgroundRequest
import io.toro.trello.response.MemberBoardBackgroundResponse
import io.toro.trello.request.MemberBoardRequest
import io.toro.trello.response.MemberBoardResponse
import io.toro.trello.request.MemberBoardFilterRequest
import io.toro.trello.response.MemberBoardInvitedResponse
import io.toro.trello.request.MemberBoardInvitedRequest
import io.toro.trello.response.MemberBoardInvitedFieldResponse
import io.toro.trello.request.MemberBoardInvitedFieldRequest

@Unroll
@Stepwise
class TrelloTest extends Specification {

	static void main( def args ) {
		
		String alias = '0ca8a7f7a0472867d3b6595c32ba6bf3:3a2e14363b51a37a0815f800bc609cbfaaa52096599ff13d636d8906d9c470d1'
		/* MEMBER API TEST
		 *
		 *
		 *
		 */
		
//		println TrelloConnector.trelloGetMember('test', '566772b08262644f8d02403e' , new MemberRequest())
//		println TrelloConnector.trelloGetMemberField('test', '566772b08262644f8d02403e' , new MemberFieldRequest(field:"avatarHash"))
//		println TrelloConnector.trelloGetMemberAction('test', '566772b08262644f8d02403e' , new MemberActionRequest())
//		println TrelloConnector.trelloGetMemberBoardBackground('test', '566772b08262644f8d02403e' , new MemberBoardBackgroundRequest())	
//		println TrelloConnector.trelloGetMemberBoardBackgroundsById('test', '566772b08262644f8d02403e' , new MemberBoardBackgroundByIdRequest(idBoardBackground:""))
//		println TrelloConnector.trelloGetMemberBoardStar('test', '566772b08262644f8d02403e')		
//		println TrelloConnector.trelloGetMemberBoard('test', '566772b08262644f8d02403e' , new MemberBoardRequest())
//		println TrelloConnector.trelloGetMemberBoardFilter('test', '566772b08262644f8d02403e' , new MemberBoardFilterRequest(filter:'members'))
//		println TrelloConnector.trelloGetMemberBoardInvited('test','566772b08262644f8d02403e', new MemberBoardInvitedRequest())
//		println TrelloConnector.trelloGetMemberBoardInvitedField('test','566772b08262644f8d02403e', new MemberBoardInvitedFieldRequest(field:"desc"))
		//println TrelloConnector.trelloGetMemberNotification('test','566772b08262644f8d02403e', new BoardActionQuery())
		//println TrelloConnector.trelloGetMemberOrganization('test','566772b08262644f8d02403e', new MemberOrganizationRequest())
		//println TrelloConnector.trelloGetMemberOrganizationFilter('test','566772b08262644f8d02403e', new MemberOrganizationFilterRequest(filter:"all"))
		//println TrelloConnector.trelloGetMemberOrganizationInvited('test','566772b08262644f8d02403e', new MemberOrganizationInvitedRequest(filter:"desc"))
		//println TrelloConnector.trelloGetMemberOrganizationInvitedField('test','566772b08262644f8d02403e', new MemberOrganizationInvitedFieldRequest(field:"desc"))
		//println TrelloConnector.trelloGetMemberSaveSearches('test','566772b08262644f8d02403e')
		//println TrelloConnector.trelloGetMemberSaveSearchesById('test','566772b08262644f8d02403e', new MemberSavedSearchesRequest(idSavedSearch:"566725a38262644f8d00c1c9"))
		//println TrelloConnector.trelloGetMemberToken('test','566772b08262644f8d02403e', new MemberTokenRequest())
		
		
		/* CHECKLIST API TEST
		 *
		 *
		 *
		 */
		
//		println TrelloConnector.trelloGetChecklist('test','5689c522bc7c5a939ddb1d4d', new ChecklistRequest())
//		println TrelloConnector.trelloGetChecklistField('test','5689c522bc7c5a939ddb1d4d', new ChecklistFieldRequest(field:"idBoard"))
//		println TrelloConnector.trelloGetChecklistBoard('test','5689c522bc7c5a939ddb1d4d', new ChecklistBoardRequest(fields:"prefs"))
//		println TrelloConnector.trelloGetChecklistBoardField('test','5689c522bc7c5a939ddb1d4d',new ChecklistBoardFieldRequest(field:"desc"))
//		println TrelloConnector.trelloGetChecklistCard('test','5689c522bc7c5a939ddb1d4d', new ChecklistCardRequest())
//		println TrelloConnector.trelloGetCardFilterByChecklist('test','5689c522bc7c5a939ddb1d4d',new ChecklistCardFilterRequest(filter:"all"))
//		println TrelloConnector.trelloGetChecklistCheckItem('test','5689c522bc7c5a939ddb1d4d', new ChecklistCheckItemRequest())
//		println TrelloConnector.trelloGetChecklistCheckItemById('test','5689c522bc7c5a939ddb1d4d', new ChecklistCheckItemByIdRequest(idCheckItem:'5689c5274914ee6d0f0a8218', fields:"name"))
//		println TrelloConnector.trelloUpdateChecklist('test','5689c522bc7c5a939ddb1d4d',new ChecklistUpdateRequest(name:"huhu",pos:1237))
//		println TrelloConnector.trelloUpdateChecklistName('test','5689c522bc7c5a939ddb1d4d',new ChecklistUpdateNameRequest(value:'rafaelmanuel'))
//		println TrelloConnector.trelloUpdateChecklistPos('test','5689c522bc7c5a939ddb1d4d',new ChecklistUpdatePosRequest(value:4312))
//		println TrelloConnector.trelloCreateChecklist('test','5689c522bc7c5a939ddb1d4d',new ChecklistCreateRequest(name:"animals",pos:666,idCard:"5672713d92786d355b033102"))
//		println TrelloConnector.trelloCreateCheckItem('test','5689c522bc7c5a939ddb1d4d', new ChecklistCreateCheckItemRequest(name:"monkey"))
//		println TrelloConnector.trelloDeleteChecklist('test','5689c522bc7c5a939ddb1d4d')
//		println TrelloConnector.trelloChecklistDeleteCheckItem('test','568b213c9063486b0837280e','5689c522bc7c5a939ddb1d4d')
		
		
		/* LIST API TEST
		 *
		 *
		 *
		 */
		
		println TrelloConnector.trelloGetLists(alias,
			'56727108f4e49d0c903f6ccf',
			 new ListRequest())
		
		println TrelloConnector.trelloGetListField(alias,'56727108f4e49d0c903f6ccf', new ListFieldRequest(field:"name"))
		
//		println TrelloConnector.trelloGetListAction('test','56727108f4e49d0c903f6ccf' , new ListActionRequest())[0]
//		println TrelloConnector.trelloGetListBoard('test','56727108f4e49d0c903f6ccf' , new ListBoardRequest())
//		println TrelloConnector.trelloGetListBoardList('test','56727108f4e49d0c903f6ccf' , new ListBoardFieldRequest())		
//		println TrelloConnector.trelloGetListCard('test', '56727108f4e49d0c903f6ccf' , new ListCardRequest())
//		println TrelloConnector.trelloGetListCardFilter('test', '56727108f4e49d0c903f6ccf' , new ListCardFilterRequest(filter:"all"))
//		println TrelloConnector.trelloUpdateList('test', '56727108f4e49d0c903f6ccf' , new ListUpdateRequest(name:"To Done"))		
//		println TrelloConnector.trelloUpdateListClosed('test', '56727108f4e49d0c903f6ccf' , new ListClosedRequest(value:true))
//		println TrelloConnector.trelloUpdateListIdBoard('test', '56727108f4e49d0c903f6ccf' , new ListIdBoardRequest(value:"5672710182645ad533697f45",pos:"1234"))
//		println TrelloConnector.trelloUpdateListName('test', '56727108f4e49d0c903f6ccf' , new ListNameRequest(value:"pogi talaga"))
//		println TrelloConnector.trelloUpdateListPos('test', '56727108f4e49d0c903f6ccf' , new ListPosRequest(value:4321))
//		println TrelloConnector.trelloUpdateListSubscribed('test', '56727108f4e49d0c903f6ccf' ,new ListSubscribedRequest(value:true))
//		println TrelloConnector.trelloCreateList('test' , new ListCreateRequest(name:"to be the three" , idBoard:"5672710182645ad533697f45"))
//		println TrelloConnector.trelloCreateListArchiveAllCard('test' , '56727108f4e49d0c903f6ccf')
//		println TrelloConnector.trelloCreateListCard('test' , '56727108f4e49d0c903f6ccf' , new ListCreateCardRequest(name:"name",desc:"desc",labels:"red" ,due:null))
		
		
//acitons		
		
//		println TrelloConnector.trelloGetAction('test','567255877c2bc8c633d62146', new ActionRequest())
//		println TrelloConnector.trelloGetActionField('test','567255877c2bc8c633d62146', new ActionFieldRequest(field:"date"))
//		println TrelloConnector.trelloGetActionBoard('test','567255877c2bc8c633d62146', new ActionBoardRequest())
//		println TrelloConnector.trelloGetActionBoardField('test','567255877c2bc8c633d62146', new ActionBoardFieldRequest(field:"desc"))
//		println TrelloConnector.trelloGetActionCard('test','567255877c2bc8c633d62146', new ActionCardRequest(fields:"desc"))
//		println TrelloConnector.trelloGetActionCardField('test','567255877c2bc8c633d62146', new ActionCardFieldRequest(field:"desc"))
//		println TrelloConnector.trelloGetActionDisplay('test','567255877c2bc8c633d62146')
//		println TrelloConnector.trelloGetActionEntities('test','567255877c2bc8c633d62146')
//		println TrelloConnector.trelloGetActionList('test','567255877c2bc8c633d62146' , new ActionListRequest(fields:"name"))
//		println TrelloConnector.trelloGetActionListField('test','567255877c2bc8c633d62146' , new ActionListFieldRequest(field:"name"))
//		println TrelloConnector.trelloGetActionMember('test','567255877c2bc8c633d62146' , new ActionMemberRequest(fields:"fullName"))
//		println TrelloConnector.trelloGetActionMemberField('test','567255877c2bc8c633d62146' , new ActionMemberFieldRequest(field:"fullName"))
//		println TrelloConnector.trelloGetActionMemberCreatorField('test','567255877c2bc8c633d62146' , new ActionMemberCreatorFieldRequest(field:"fullName"))
//		println TrelloConnector.trelloGetActionOrganization('test','567255877c2bc8c633d62146' , new ActionOrganizationRequest())
//		println TrelloConnector.trelloGetActionOrganizationField('test','567255877c2bc8c633d62146' , new ActionOrganizationFieldRequest(field:"desc"))
//		println TrelloConnector.trelloUpdateAction('test','567255877c2bc8c633d62146' , new ActionTextRequest())
//		println TrelloConnector.trelloUpdateActionText('test','567255877c2bc8c633d62146' , new ActionTextValueRequest(value:"hello"))

//board and card		
//		println TrelloConnector.trelloGetBoard( 'test', '567255302b6a578f8e9f61c9' )
//		println TrelloConnector.trelloGetBoardCards( 'test', '55dfe91329843341f9678942',
//			new BoardCardQuery( stickers: true ) )
//		println TrelloConnector.trelloGetBoardChecklists( 'test', '55dfefd440f6eac0df566688',
//			new BoardChecklistQuery( cards: 'all' ) )
//		println TrelloConnector.trelloGetBoardLabels( 'test', '55dfefd440f6eac0df566688', '', 50 )
//		println TrelloConnector.trelloGetBoardLabelById( 'test', '55dfefd440f6eac0df566688',
//			'55dfefd419ad3a5dc2416e0b', '' )
//		println TrelloConnector.trelloGetBoardMembershipById( 'test', '55dfefd440f6eac0df566688',
//			'55dfefd440f6eac0df56668c', true, '' )
//		println TrelloConnector.trelloGetCard( 'test', '55e52fec43cd560c659cc9dc',
//			new CardQuery( actions: 'all', members: true, actionsEntities: true, membersVoted: true, board: true, list: true,
//				listFields: 'all', stickers: true ) )55e906749e1c0a82786d0110
//		println TrelloConnector.trelloCloseCard( 'test', '55e52fec43cd560c659cc9dc', false )
//		println TrelloConnector.trelloMoveCardToSpecificList( 'test', '55e52fec43cd560c659cc9dc',
//			'55e906749e1c0a82786d0110' )
//		println TrelloConnector.trelloCardAddMembers( 'test', '55e52fec43cd560c659cc9dc',
//			[ '55dfab633cd2da54326bcbdb' ] )
//		println TrelloConnector.trelloCardUpdateLabel( 'test', '55e52fec43cd560c659cc9dc',
//			'all' )
//		println TrelloConnector.trelloCardUpdateName( 'test', '55e52fec43cd560c659cc9dc',
//			'updated again' )
//		println TrelloConnector.trelloCardUpdatePos( 'test', '55e52fec43cd560c659cc9dc',
//			'bottom' )
//		println TrelloConnector.trelloCardUpdateSticker( 'test', '55e52fec43cd560c659cc9dc',
//			'55e7d390233cfdaa99fb9893', 1, 4, 1, 50 )
//		println TrelloConnector.trelloSubscribeCard( 'test', '55e52fec43cd560c659cc9dc',
//			true )
	//		println TrelloConnector.trelloCreateCard( 'test', new AddCard( name: 'sample card',
	//			desc: 'sample description', due: new Date().plus( 1 ).format( 'yyyy-MM-dd HH:mm:ss',
	//				TimeZone.getTimeZone( 'UTC' ) ).replace( ' ', 'T' ), idList: '55e52fe47e7f851f58f6316e'))//,
//			fileSource: new File( '/Users/charlesturla/Desktop/enlogo.png' ).bytes.encodeBase64(), urlSource: 'null' ) )
//		println TrelloConnector.trelloCardUpdateDescription( 'test', '55e52fec43cd560c659cc9dc', 'test again' )
//		println TrelloConnector.trelloCardUpdateAttachmentCover( 'test', '55e52fec43cd560c659cc9dc', '' )
//		println TrelloConnector.trelloMoveCardToSpecificBoard( 'test', '55e52fec43cd560c659cc9dc',
//			'55dfe91329843341f9678942', '55e52fe47e7f851f58f6316e' )
//		println TrelloConnector.trelloCardUpdateDueDate( 'test', '55e52fec43cd560c659cc9dc', '' )
//		println TrelloConnector.trelloCardGetMembers( 'test', '55e52fec43cd560c659cc9dc', '' )
//		println TrelloConnector.trelloCardUpdateComment( 'test', '55e52fec43cd560c659cc9dc',
//			'55e8eaca464f522b41f58c5cad', 'updated again' )
//		println TrelloConnector.trelloCardUpdateCheckItem( 'test', '55e52fec43cd560c659cc9dc',
//			'55e7e409583b6dde17d4f9be', '55e7e411f5568888d9cb2edb', '', '', 'incomplete' )
//		println TrelloConnector.trelloCardUpdatedCheckItemName( 'test', '55e52fec43cd560c659cc9dc',
//			'55e7e409583b6dde17d4f9be', '55e7e411f5568888d9cb2edb', 'TODO2: updated again' )
//		println TrelloConnector.trelloCardUpdateCheckItemPos( 'test', '55e52fec43cd560c659cc9dc',
//			'55e7e409583b6dde17d4f9be', '55e7e411f5568888d9cb2edb', 'top' )
//		println TrelloConnector.trelloCardUpdateCheckItemState( 'test', '55e52fec43cd560c659cc9dc',
//			'55e7e409583b6dde17d4f9be', '55e7e411f5568888d9cb2edb', 'incomplete' )
//		println TrelloConnector.trelloCardGetStickers( 'test', '55e52fec43cd560c659cc9dc', '' )
//		println TrelloConnector.trelloCardGetSpecificSticker( 'test', '55e52fec43cd560c659cc9dc',
//			'55e7d390233cfdaa99fb9893', '' )
//		println TrelloConnector.trelloUpdateCard( 'test', '55e52fec43cd560c659cc9dc',
//			new UpdateCard( closed: false, name: 'updated name', desc: 'updated desc', pos: '2' ) )
//		println TrelloConnector.trelloCardGetListbyField( 'test', '55e52fec43cd560c659cc9dc', 'name' )
//		println TrelloConnector.trelloCardGetCheckItemStates( 'test', '55e52fec43cd560c659cc9dc', '' )
//		println TrelloConnector.trelloCardGetChecklists( 'test', '55e52fec43cd560c659cc9dc', new BoardQuery() )
//		println TrelloConnector.trelloCardGetAttachments( 'test', '55e52fec43cd560c659cc9dc', '' )
//		println TrelloConnector.trelloCardGetBoard( 'test', '55e52fec43cd560c659cc9dc', '' )
//		println TrelloConnector.trelloBoardAddMember( 'test', '55dfefd440f6eac0df566688',
//			'turlacharlesirvin@gmail.com', 'Charles Turla', '' )
//		println TrelloConnector.trelloBoardMarkAsViewed( 'test', '55dfefd440f6eac0df566688' )
//		println TrelloConnector.trelloBoardDeleteMember( 'test', '55dfefd440f6eac0df566688', '' )
//		println TrelloConnector.trelloBoardAddPowerUp( 'test', '55dfefd440f6eac0df566688', 'calendar' )
//		println TrelloConnector.trelloBoardRemovePowerUp( 'test', '55dfe91329843341f9678942', 'calendar' )
//		println TrelloConnector.trelloBoardAddList( 'test', '55dfefd440f6eac0df566688', 'Test1', 'top' )
//		println TrelloConnector.trelloBoardAddLabel( 'test', '55dfefd440f6eac0df566688', 'test', 'sky' )
//		println TrelloConnector.trelloBoardAddChecklist( 'test', '55dfefd440f6eac0df566688', 'test-again' )
//		println TrelloConnector.trelloBoardGenerateEmailKey( 'test', '55dfefd440f6eac0df566688' )
//		println TrelloConnector.trelloBoardUpdateMember( 'test', '55dfefd440f6eac0df566688',
//			'55e690b98f1e321ab5a5b9fa', 'admin' )
//		println TrelloConnector.trelloSubscribeBoard( 'test', '55dfefd440f6eac0df566688', true )
//		println TrelloConnector.trelloBoardUpdateMembership( 'test', '55dfefd440f6eac0df566688',
//			'55e690b98f1e321ab5a5b9fc', 'admin', 'all' )
//		println TrelloConnector.trelloBoardUpdatePrefsEmailPos( 'test', '55dfefd440f6eac0df566688', 'top' )
//		println TrelloConnector.trelloBoardUpdatePrefsEmailList( 'test', '55dfefd440f6eac0df566688',
//			'55dfefd440f6eac0df566689' )
//		println TrelloConnector.trelloBoardGenerateCalendarKey( 'test', '55dfefd440f6eac0df566688' )
//		println TrelloConnector.trelloBoardUpdatePrefsShowSidebarMembers( 'test', '55dfefd440f6eac0df566688', true )
//		println TrelloConnector.trelloBoardUpdatePrefsShowSidebarBoardAct( 'test', '55dfefd440f6eac0df566688', false )
//		println TrelloConnector.trelloBoardUpdatePrefsShowListGuide( 'test', '55dfefd440f6eac0df566688', false )
//		println TrelloConnector.trelloBoardUpdatePrefsShowSidebarAct( 'test', '55dfefd440f6eac0df566688', false )
//		println TrelloConnector.trelloBoardUpdatePrefsShowSidebar( 'test', '55dfefd440f6eac0df566688', false )
//		println TrelloConnector.trelloUpdateBoard( 'test', '55dfefd440f6eac0df566688', new UpdateBoard(
//			desc: 'updated description', prefsPermissionLevel: 'public', labelNameGreen: 'this is green' ) )
//		println TrelloConnector.trelloGetBoardOrgByField( 'test', '55dfefd440f6eac0df566688', 'url' )
//		println TrelloConnector.trelloGetBoardOrganization( 'test', '55dfefd440f6eac0df566688', '' )
//		println TrelloConnector.trelloGetBoardPrefs( 'test', '55dfefd440f6eac0df566688' )
//		println TrelloConnector.trelloGetBoardMemberships( 'test', '55dfefd440f6eac0df566688', '', true, '' )
//		println TrelloConnector.trelloGetBoardMembersInvited( 'test', '55dfefd440f6eac0df566688', '' )
//		println TrelloConnector.trelloGetBoardMembers( 'test', '55dfefd440f6eac0df566688', 'all', 'all', true )
//		println TrelloConnector.trelloGetBoardMembersByFilter( 'test', '55dfefd440f6eac0df566688', 'admins' )
//		println TrelloConnector.trelloGetBoardMemberCards( 'test', '55dfefd440f6eac0df566688', '55dfab633cd2da54326bcbdb' )
//		println TrelloConnector.trelloGetBoardListsByFilter( 'test', '55dfefd440f6eac0df566688', 'closed' )
//		println TrelloConnector.trelloGetBoardLists( 'test', '55dfefd440f6eac0df566688', new ListQuery() )
//		println TrelloConnector.trelloGetBoardCardById( 'test', '55dfefd440f6eac0df566688',
//			'55e00527afd702bd7ffb7d0f', new CardQuery() )
//		println TrelloConnector.trelloGetBoardCardByFilter( 'test', '55dfefd440f6eac0df566688', 'closed' )
//		println TrelloConnector.trelloGetActionsByBoard( 'test', '567255302b6a578f8e9f61c9',
//			new BoardActionQuery( format: '' ) )
//		println TrelloConnector.trelloGetBoardStars( 'test', '55dfefd440f6eac0df566688', '' )
//		println TrelloConnector.trelloCreateBoard( 'test', new AddBoard( name: 'Test Board' ) )
//		println TrelloConnector.trelloDeleteBoard( 'test', '55dfefd440f6eac0df566688',
//				'charlesirvinturla' )
//		println TrelloConnector.trelloCloseBoard( 'test', '55dfefd440f6eac0df566688', true )
//		println TrelloConnector.trelloGetFieldsByBoard( 'test', '55dfefd440f6eac0df566688', 'prefs' )
//		println TrelloConnector.trelloCreateBoard( 'test', new AddBoard( name: 'Dev1 board',
//				desc: 'This is a test board v2', prefsPermissionLevel: 'private' ) )
		
		
		
		/* LABEL API TEST
		 * 
		 * 
		 * 
		 */
		
		//println TrelloConnector.trelloGetLabel('test','56727101fb396fe706e7618e', new LabelQuery())
		//println TrelloConnector.trelloGetLabelBoard('test','56727101fb396fe706e7618e','')
		//println TrelloConnector.trelloGetLabelBoardField('test','56727101fb396fe706e7618e','name')
		//println TrelloConnector.trelloUpdateLabel('test','56727101fb396fe706e7618e', new LabelQuery(name:"tarzan",color:"pink"))
		//println TrelloConnector.trelloUpdateLabelColor('test','56727101fb396fe706e7618e', 'pink')
		//println TrelloConnector.trelloUpdateLabelName('test','56727101fb396fe706e7618e', 'barok')
		//println TrelloConnector.trelloCreateLabel('test','saitama', 'yellow','5672710182645ad533697f45')
		//println TrelloConnector.trelloDeleteLabel('test','568b4b2f7b81e2e918a4865b')
		
		
		
		
		
		/* NOTIFICATION API TEST
		 *
		 *
		 *
		 */
		
		//println TrelloConnector.trelloGetNotification('test','56935487343a63741282dc3a', new BoardActionQuery())
		//println TrelloConnector.trelloGetNotificationField('test','56935487343a63741282dc3a', new NotificationFieldRequest(field:'type'))
		//println TrelloConnector.trelloGetNotificationEntities('test','56935487343a63741282dc3a')
		//println TrelloConnector.trelloGetNotificationMemberCreator('test','56935487343a63741282dc3a' , new NotificationMemberCreatorRequest())
		//println TrelloConnector.trelloGetNotificationMemberCreatorField('test','56935487343a63741282dc3a' , new NotificationMemberCreatorFieldRequest(field:"email"))
		//println TrelloConnector.trelloGetNotificationOrganization('test','56935487343a63741282dc3a' ,new NotificationOrganizationRequest(fields:"name"))
		//println TrelloConnector.trelloGetNotificationOrganizationField('test','56935487343a63741282dc3a' ,new NotificationOrganizationFieldRequest(field:"name"))
		//println TrelloConnector.trelloUpdateNotification('test','56935487343a63741282dc3a' ,new NotificationRequest())
		//println TrelloConnector.trelloUpdateNotificationFieldUnread('test','56935487343a63741282dc3a' ,new NotificationFieldUnreadRequest(value:true))
		//println TrelloConnector.trelloCreateNotification('test')
		
		
		/* SEARCH API TEST
		 *
		 *
		 *
		 */
		
		//println TrelloConnector.trelloGetSearch('test',new SearchRequest(query:'cards'))
		//println TrelloConnector.trelloGetSearchMember('test',new SearchMemberRequest(query:'board'))
		
		/* TYPE API TEST
		 *
		 *
		 *
		 */
		//println TrelloConnector.trelloGetType('test',"rafaelmanuel")
		
		/* ORGANIZATIONS API TEST
		 *
		 *
		 *
		 */
		
		//println TrelloConnector.trelloGetOrganization('test',"56735b6e0c75d274b791dac6" , new OrganizationQuery(members:"all"))
		//println TrelloConnector.trelloGetOrganizationField('test','56735b6e0c75d274b791dac6','desc')
		//println TrelloConnector.trelloGetOrganizationActions('test' , '56735b6e0c75d274b791dac6' , new RequestOrganizationActions())[0]
		//println TrelloConnector.trelloGetOrganizationBoards('test' , '56735b6e0c75d274b791dac6' , new OrganizationBoardRequest(fields:"desc"))
		//println TrelloConnector.trelloGetOrganizationBoardFilter('test' , '56735b6e0c75d274b791dac6' , "members")
		//println TrelloConnector.trelloGetOrganizationMembers('test' , '56735b6e0c75d274b791dac6')
		//println TrelloConnector.trelloGetOrganizationMembersByFilter('test' , '56735b6e0c75d274b791dac6' , "admins")
		//println TrelloConnector.trelloGetOrganizationMemberships('test' , '56735b6e0c75d274b791dac6')
		//println TrelloConnector.trelloGetOrganizationMembershipById('test' , '56735b6e0c75d274b791dac6' , '56735b6e0c75d274b791dac7')
		//println TrelloConnector.trelloUpdateOrganizationDesc('test' , '56735b6e0c75d274b791dac6' , 'Hello')
		//println TrelloConnector.trelloUpdateOrganizationDisplayName('test' , '56735b6e0c75d274b791dac6' , 'Hello')
		//println TrelloConnector.trelloUpdateOrganizationMember('test' , '56735b6e0c75d274b791dac6' , 'blank.fvk@gmail.com', 'Shiina Mashiro','normal')
		//println TrelloConnector.trelloUpdateOrganizationMemberType('test' , '56735b6e0c75d274b791dac6' , '5673597182f5e9637586b77e', 'admin')
		//println TrelloConnector.trelloUpdateOrganizationDeactivated('test' , '56735b6e0c75d274b791dac6' , '5673597182f5e9637586b77e', false)
		//println TrelloConnector.trelloUpdateOrganizationMembership('test' , '56735b6e0c75d274b791dac6' , '56735b6e0c75d274b791dac7', 'admin' , '')
		//println TrelloConnector.trelloUpdateOrganizationName('test' , '56735b6e0c75d274b791dac6' , 'jejemon',)
		//println TrelloConnector.trelloUpdateOrganizationAssociatedDomain('test' , '56735b6e0c75d274b791dac6' , 'jejemon',)
		//println TrelloConnector.trelloUpdateOrganizationBoardVisibilityRestrictOrg('test' , '56735b6e0c75d274b791dac6' , 'admin',)
		//println TrelloConnector.trelloUpdateOrganizationBoardVisibilityRestrictPrivate('test' , '56735b6e0c75d274b791dac6' , 'admin',)
		//println TrelloConnector.trelloUpdateOrganizationBoardVisibilityRestrictPublic('test' , '56735b6e0c75d274b791dac6' , 'admin',)
		
		/* TOKEN API TEST
		 *
		 *
		 *
		 */
		
		//println TrelloConnector.trelloGetToken('test' , '3a2e14363b51a37a0815f800bc609cbfaaa52096599ff13d636d8906d9c470d1')
		//println TrelloConnector.trelloGetTokenField('test' , '3a2e14363b51a37a0815f800bc609cbfaaa52096599ff13d636d8906d9c470d1' , 'idMember')
		//println TrelloConnector.trelloGetTokenMember('test' , '3a2e14363b51a37a0815f800bc609cbfaaa52096599ff13d636d8906d9c470d1' , 'fullName')
		//println TrelloConnector.trelloGetTokenMemberField('test' , '3a2e14363b51a37a0815f800bc609cbfaaa52096599ff13d636d8906d9c470d1' , 'fullName')
		//println TrelloConnector.trelloGetTokenWebhook('test' , '3a2e14363b51a37a0815f800bc609cbfaaa52096599ff13d636d8906d9c470d1')
		//println TrelloConnector.trelloGetTokenWebhookById('test' , '3a2e14363b51a37a0815f800bc609cbfaaa52096599ff13d636d8906d9c470d1' , '56933ea73c4ea18a69d1ec1d')
		//println TrelloConnector.trelloUpdateTokenWebhook('test' , '3a2e14363b51a37a0815f800bc609cbfaaa52096599ff13d636d8906d9c470d1' , new TokenWebhookRequest(description:"hello",idModel:"56933ea73c4ea18a69d1ec1d",callbackURL:"http://www.google.com"))
		//println TrelloConnector.trelloCreateTokenWebhook('test' , '3a2e14363b51a37a0815f800bc609cbfaaa52096599ff13d636d8906d9c470d1' , new TokenWebhookRequest(description:"hello",idModel:"567255302b6a578f8e9f61c9",callbackURL:"http://www.facebook.com"))
		
		
		/* WEBHOOK API TEST
		 *
		 *
		 *
		 */
		
		//println TrelloConnector.trelloGetWebhookField('test' , '56933ea73c4ea18a69d1ec1d' , 'active')
		//println TrelloConnector.trelloUpdateWebhook('test' , '56933ea73c4ea18a69d1ec1d' , new TokenWebhookRequest(description:"hello",idModel:"567255302b6a578f8e9f61c9",callbackURL:"http://www.facebook.com"))
		//println TrelloConnector.trelloUpdateWebhookActive('test' , '56933ea73c4ea18a69d1ec1d' , true)
		}
	
	@Shared
		alias = 'test', boardId = ''
		
	def 'create member'(){
		setup:
			MemberResponse member = TrelloConnector.trelloGetMember(alias , "566772b08262644f8d02403e" ,new MemberRequest())
		expect:
			member.username == 'rafaelmanuel2'
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	def 'create board'() {
//		setup:
//			Boards board = TrelloConnector.trelloCreateBoard( alias, new AddBoard( name: 'Dev board',
//				desc: 'This is a test board' ) )
//			boardId = board.id
//		expect:
//			board.name == 'Dev board'
//			board.closed == false
//			board.desc == 'This is a test board'
//	}
//
//	def 'get board'() {
//		setup:
//			Boards board = TrelloConnector.trelloGetBoard( alias, boardId )
//		expect:
//			board.name == 'Trello Development'
//	}
//
//	def 'get fields on board'() {
//		setup:
//			def result = TrelloConnector.trelloGetFieldsByBoard( alias, boardId, 'prefs' )
//		expect:
//			result.permissionLevel == 'private'
//	}
//
//	def 'get actions by board'() {
//		setup:
//			BoardAction[] result = TrelloConnector.trelloGetActionsByBoard( alias, boardId, new BoardActionQuery( entities: true ) )
//		expect:
//			result.find { it.entities.find { it.type == 'member' }.username == 'charlesirvinturla' }
//	}
//
//	def 'get board stars'() {
//		setup:
//			BoardStars[] list = TrelloConnector.trelloGetBoardStars( alias, '55dfefd440f6eac0df566688', '' )
//		expect:
//			list.find { it.idBoard == '55dfefd440f6eac0df566688' }.id == '55e5157336c1d3ca2606c0a9'
//	}
//
//	def 'get board cards'() {
//		setup:
//			BoardCard[] list = TrelloConnector.trelloGetBoardCards( 'test', '55dfefd440f6eac0df566688',
//			new BoardCardQuery( stickers: true ) )
//		expect:
//			list.find { it.name == 'Create a Google Calendar event from this card' }.stickers.image =~ /star/
//	}
//
//	def 'get board cards by filter'() {
//		setup:
//			BoardCard[] list = TrelloConnector.trelloGetBoardCardByFilter( 'test', '55dfefd440f6eac0df566688', 'closed' )
//		expect:
//			list.find { it.name == 'sample card' }.closed == true
//	}
//
//	def 'get board card by id'() {
//		setup:
//			BoardCard card = TrelloConnector.trelloGetBoardCardById( alias, '', '',  new CardQuery() )
//		expect:
//			card.name == ''
//	}
//
//	def 'get board checklists'() {
//		setup:
//			BoardChecklist[] list = TrelloConnector.trelloGetBoardChecklists( alias, '55dfefd440f6eac0df566688',
//				new BoardChecklistQuery( cards: 'all' ) )
//		expect:
//			list.find { it.name == 'Checklist' }.checkItems.name =~ /TODO: 1/
//	}
//
//	@Shared
//		labelId = ''
//	def 'get board labels'() {
//		setup:
//			Label[] labels = TrelloConnector.trelloGetBoardLabels( alias, boardId, '', 50 )
//			labelId = labels.find { it.color == 'green' }
//		expect:
//			labels.find { it.color == 'green' }.idBoard == boardId
//	}
//
//	def 'get board label by id'() {
//		setup:
//			Label label = TrelloConnector.trelloGetBoardLabelById( alias, boardId, labelId, '' )
//		expect:
//			label.color == 'green'
//			label.idBoard == boardId
//	}
//
//	def 'get board lists'() {
//		setup:
//			BoardList[] list = TrelloConnector.trelloGetBoardLists( alias, '', new ListQuery() )
//		expect:
//			list.find { it.name == '' }.close == false
//			list.find { it.name == '' }.subscribed == false
//	}
//
//	def 'add board member'() {
//		setup:
////			BoardMember member = 
//			BoardMember[] brdMembers = TrelloConnector.trelloGetBoardMembers( alias, boardId, 'all', 'all', true )
//		expect:
//			brdMembers.find { it.initials == 'CT' }.fullName == 'Charles Irvin Turla'
//	}
//
//	def 'get board members by filer'() {
//		setup:
//			BoardMember[] brdMembers = TrelloConnector.trelloGetBoardMembersByFilter( alias, boardId, 'admins' )
//		expect:
//			brdMembers.find { it.username == 'charlesirvinturla' }.fullName == 'Charles Irvin Turla'
//	}
//
//	def 'close a board'() {
//		setup:
//			Boards result = TrelloConnector.trelloCloseBoard( alias, '55dfefd440f6eac0df566688', true )
//		expect:
//			result.closed == true
//	}
}
